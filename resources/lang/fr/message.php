<?php
return [
    "status" => [
        \App\Status::RECLAMATION_CREEE => 'Créée et non affectée',
        \App\Status::RECLAMATION_TRAITEE_TERMINE => 'Clôturée',
        \App\Status::RECLAMATION_TRANSFEREE_EQUIPE => 'Affectée aux équipes',
    ],

];