@extends('layouts._layout')

@section('content')
    <style type="text/css" href="{{request()->getBaseUrl()}}/plugins/datepicker/datepicker3.css"> </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-6 col-xs-12">Periode du : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodedebut" value="{{$collection->get('dateDebut')->format('d/m/Y')}}"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-2 col-sm-6 col-xs-6"> au : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodefin" value="{{$collection->get('dateFin')->format('d/m/Y')}}"/>
                                    </div>
                                </div>
                                <button class="btn btn-success" type="submit">Rechercher</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-gears"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total dépannages</span>
                        <span class="info-box-number">{{$reclamations->count()}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-stethoscope"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Dépannages en cours</span>
                        <span class="info-box-number">{{$reclamations->where('status','<>',\App\Status::RECLAMATION_TRAITEE_TERMINE)->count()}}</span>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-thumbs-o-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Dépannages terminés</span>
                        <span class="info-box-number">{{$reclamations->where('status',\App\Status::RECLAMATION_TRAITEE_TERMINE)->count()}}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-spinner"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Dépannages non affectés</span>
                        <span class="info-box-number">{{$reclamations->where('status',\App\Status::RECLAMATION_CREEE)->count()}}</span>
                    </div>
                </div>
            </div>
        </div>

            <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cartographie de dépannages</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="map" style="height: 600px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Derniers dépannages</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Numéro</th>
                                    <th>Client</th>
                                    <th>Status</th>
                                    <th>Urgence</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reclamations as $reclamation)
                                <tr>
                                    <td><a href="javascript:void(0);">{{$reclamation->numero}}</a></td>
                                    <td>{{$reclamation->client->nom}} {{$reclamation->client->prenoms}}</td>
                                    <td>
                                        {{\App\Status::getString($reclamation->status)}}
                                    </td>
                                    <td>
                                        @if($reclamation->priorite_id == \App\Priorite::PRIORITE_NORMAL)<span class="label label-info">Normal</span>@endif
                                        @if($reclamation->priorite_id == \App\Priorite::PRIORITE_URGENT)<span class="label label-warning">Urgent</span>@endif
                                        @if($reclamation->priorite_id == \App\Priorite::PRIORITE_TRES_URGENT)<span class="label label-danger">Très urgent</span>@endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <a href="{{route('liste_depannages')}}" class="btn btn-sm btn-default btn-flat pull-right">Voir tout ...</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
<!-- ChartJS 1.0.1 -->
<script src="{{request()->getBaseUrl()}}/plugins/chartjs/Chart.min.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{request()->getBaseUrl()}}/plugins/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript" >

$(function () {
    //Date picker
    $('.calendar').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'fr'
    });
});

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 5.330306, lng: -3.9763826}, //Abidjan
        zoom: 12
    });

    @foreach($reclamations as $reclamation)
    var marker{{$reclamation->id}} = new google.maps.Marker({
        map: map,
        title: 'Dépannage N°{{$reclamation->numero}}, affectée à {{$reclamation->appareil ? $reclamation->appareil->libelle : 'Aucune équipe'}}',
        animation: google.maps.Animation.DROP,
        position: {lat:parseFloat('{{$reclamation->client->latitude}}'),lng:parseFloat('{{$reclamation->client->longitude}}')} ,
        icon :'{{request()->getBaseUrl()}}/panne.png'
    });

    @endforeach
}

$(function () {

    'use strict';

    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-----------------------
    //- MONTHLY SALES CHART -
    //-----------------------

    // Get context with jQuery - using jQuery's .get() method.
    var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var salesChart = new Chart(salesChartCanvas);

   /* var salesChartData = {
        labels: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet","Août","Septembre","Octobre","Novembre","Décembre"],
        datasets: [
            {
                label: "Très Urgent",
                fillColor: "rgb(252, 70, 70)",
                strokeColor: "rgb(252, 70, 70)",
                pointColor: "rgb(252, 70, 70)",
                pointStrokeColor: "#fc4646",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgb(220,220,220)",
                data: [25, 56, 42, 81, 56, 55, 40]
            },
            {
                label: "Urgent",
                fillColor: "rgba(247, 163, 74,0.9)",
                strokeColor: "rgba(247, 163, 74,0.8)",
                pointColor: "#f7a34a",
                pointStrokeColor: "rgba(247, 163, 74,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(247, 163, 74,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label: "Normal",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#8edfff",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [36, 79, 52, 15, 65, 88, 27]
            }
        ]
    };
    */

    var salesChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&callback=initMap"
        async defer>
</script>
@endsection