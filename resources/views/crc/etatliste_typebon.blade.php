@extends('layouts._layout')

@section('content')
    <style type="text/css" href="{{ asset("/plugins/datepicker/datepicker3.css") }}"> </style>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <form class="form-horizontal" method="get" action="">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4">Periode du : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodedebut" value="{{ request('periodedebut') ? request('periodedebut') : Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-1 col-sm-4 col-xs-4"> au : </label>
                                <div  class="col-md-2 col-sm-2 col-xs-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodefin" value="{{ request('periodefin') ? request('periodefin') : Carbon\Carbon::now()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i>Rechercher</button>
                                <a href="?{{ http_build_query(array_add(request()->query(),"print","pdf")) }}" class="btn btn-default text-color-red"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                <a href="?{{ http_build_query(array_add(request()->query(),"print","excel")) }}" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Excel</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Stats des Bons -->
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><div class="text-center">Numero Réclamation</div></th>
                                <th><div class="text-center">Nom Abonné</div></th>
                                <th><div class="text-center">Nom de l'Appelant</div></th>
                                <th><div class="text-center">Date de l'Appel</div></th>
                                <th><div class="text-center">Affectation</div></th>
                                <th><div class="text-center">Type de Panne</div></th>
                                <th><div class="text-center">Exploitation</div></th>
                                <th><div class="text-center">Direction</div></th>
                                <th><div class="text-center">Priorité</div></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bons as $reclamation)
                                <tr>
                                    <td align="center">{{$reclamation->numero}}</td>
                                    <td align="center">{{$reclamation->nom." ".$reclamation->prenoms}}</td>
                                    <td align="center">{{$reclamation->nomappelant ? $reclamation->nomappelant : $reclamation->nom." ".$reclamation->prenoms}}</td>
                                    <td align="center">{{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y')}}</td>
                                    <td align="center">{{$reclamation->statut}}</td>
                                    <td align="center">{{$reclamation->pannelib}}</td>
                                    <td align="center">{{$reclamation->exploitationlib}}</td>
                                    <td align="center">{{$reclamation->libelle}}</td>
                                    <td align="center">{{$reclamation->prioritelib}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@section('script')
<!-- DataTables -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset("/plugins/datepicker/bootstrap-datepicker.js") }}"></script>

<script type="text/javascript" >
    $(function () {
        //Date picker
        $('.calendar').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'fr'
        });
        //DataTables
        $("#exploitation").DataTable();
    });
</script>
@endsection

