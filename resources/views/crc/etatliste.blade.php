@extends('layouts._layout')

@section('content')
    <style type="text/css" href="{{request()->getBaseUrl()}}/plugins/datepicker/datepicker3.css"> </style>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <form class="form-horizontal" method="get" action="">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4">Periode du : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodedebut" value="{{ request('periodedebut') ? request('periodedebut') : Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-1 col-sm-4 col-xs-4"> au : </label>
                                <div  class="col-md-2 col-sm-2 col-xs-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodefin" value="{{ request('periodefin') ? request('periodefin') : Carbon\Carbon::now()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <button class="btn btn-success" type="submit">Rechercher</button>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Stats des Bons -->
                <div class="box box-primary">
                    <div class="box-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Numero Réclamation</th>
                            <th>Nom Abonné</th>
                            <th>Nom de l'Appelant</th>
                            <th>Date de l'Appel</th>
                            <th>Type de Panne</th>
                            <th>Exploitation</th>
                            <th>Priorité</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reclamations as $reclamation)
                        <tr>
                            <td>{{$reclamation->numero}}</td>
                            <td>{{$reclamation->client->nom." ".$reclamation->client->prenoms}}</td>
                            <td> {{$reclamation->nomappelant ? $reclamation->nomappelant : $reclamation->client->nom." ".$reclamation->client->prenoms}}</td>
                            <td>{{$reclamation->datereclamation}}</td>
                            <td>{{$reclamation->panne->libelle}}</td>
                            <td>{{$reclamation->exploitation->libelle}}</td>
                            <td>{{$reclamation->priorite->libelle}}</td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                   {{"nombre de lignes totales"." ". $reclamations->total()}}
                    <br>
                    {{ $reclamations->appends(request()->input())->links() }}
                </div>
                </div>



            </div>
        </div>
    </section>
@endsection
@section('script')
    <!-- DataTables -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{request()->getBaseUrl()}}/plugins/datepicker/bootstrap-datepicker.js"></script>

    <script type="text/javascript" >

        $(function () {
            //Date picker
            $('.calendar').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'fr'
            });

            //DataTables
            $("#exploitation").DataTable();
        });
    </script>
@endsection

