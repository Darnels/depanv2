@extends('layouts._layout')

@section('content')
    <style type="text/css" href="{{request()->getBaseUrl()}}/plugins/datepicker/datepicker3.css"> </style>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-gears"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Appels du mois</span>
                        <span class="info-box-number">{{$reclamation->get("reclamationMensuel")}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="fa fa-gears"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Annuel</span>
                        <span class="info-box-number">{{$reclamation->get("reclamationAnnuel")}}</span>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-tachometer"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Ratio Mensuel</span>
                        @if($reclamation->get("reclamationAnnuel") != 0)
                        <span class="info-box-number">{{ number_format(($reclamation->get("reclamationMensuel")/$reclamation->get("reclamationAnnuel"))*100,2) }}%</span>
                        @else
                        <span class="info-box-number">00%</span>
                        @endif
                    </div>
                </div>
            </div>

            <!-- Stats des Bons -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bons transmis</span>
                        <span class="info-box-number">{{ $bon->transmis }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-thumbs-o-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bons exécutés</span>
                        <span class="info-box-number">{{ $bon->executes }}</span>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-clock-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">DMD - Délai Moyen de Dépannage</span>
                        <span class="info-box-number">{{ $bon->moyenne }}</span>
                    </div>
                </div>
            </div>


            <!-- Stats des Bons -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('rapport_mai')}}"><span class="info-box-icon bg-blue"><i class="fa fa-newspaper-o"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">BONS PASSES EN MAINTENANCE</span>
                        <span class="info-box-number">{{ $bons->maintenance }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('rapport_pro')}}"><span class="info-box-icon bg-blue"><i class="fa fa-newspaper-o"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">BONS PASSES EN PROVISOIRE</span>
                        <span class="info-box-number">{{ $bons->provisoire }}</span>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('rapport_def')}}"><span class="info-box-icon bg-blue"><i class="fa fa-newspaper-o"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">BONS CLOTURES</span>
                        <span class="info-box-number">{{ $bons->definitif }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dépannage par famille</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <canvas id="pieChartFamille" style="height: 393px; width: 787px;" height="393" width="787"></canvas>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dépannage par nature</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <select id="famille" class="form-control">
                            @foreach($inputs->get('familles') as $famille)
                                <option value="{{$famille->code}}">{{$famille->libelle}}</option>
                            @endforeach
                        </select>
                        <canvas id="barChartNature" style="height: 393px; width: 787px;" height="393" width="787"></canvas>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<!-- ChartJS 1.0.1 -->
<script src="{{request()->getBaseUrl()}}/plugins/chartjs/Chart.min.js"></script>
<script type="application/javascript">
//-------------
//- BAR CHART -
//-------------
var AJAX_FAMILLE_NATURE_URL = '{{ route('ajax_famille_nature',['code' => '_#_']) }}';
var barChartData = null;

var barChartCanvas = $("#barChartNature").get(0).getContext("2d");
var barChart = new Chart(barChartCanvas);
var barChartOptions = {
    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - If there is a stroke on each bar
    barShowStroke: true,
    //Number - Pixel width of the bar stroke
    barStrokeWidth: 2,
    //Number - Spacing between each of the X value sets
    barValueSpacing: 5,
    //Number - Spacing between data sets within X values
    barDatasetSpacing: 1,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    //barChartOptions.datasetFill = false;
    //barChart.Bar(barChartData, barChartOptions);

//Ajax sur selection de la famille
$('#famille').change(function () {
    $.getJSON(AJAX_FAMILLE_NATURE_URL.replace('_#_',$(this).val()),function (data) {
        console.log(data.libelles);
        barChartData = {
            labels: data.libelles,
            datasets: [
                {
                    label: "Natures",
                    fillColor: "rgba(0, 166, 90,1)",
                    strokeColor: "rgba(210, 214, 222, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: data.total
                }
            ]
        };

        barChartOptions.datasetFill = false;
        barChart.Bar(barChartData, barChartOptions);
    });
});

//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
var pieChartCanvas = $("#pieChartFamille").get(0).getContext("2d");
var pieChart = new Chart(pieChartCanvas);
var color = ["#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de",];
var PieData = [
    @foreach($familly as $k => $v)
    {
        value: {{ $v->total }},
        color: color[{{$k}}],
        highlight: color[{{$k}}],
        label: "{{ $v->libelle }}"
    },
    @endforeach
];
var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 2,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
};
pieChart.Doughnut(PieData, pieOptions);
</script>
@endsection

