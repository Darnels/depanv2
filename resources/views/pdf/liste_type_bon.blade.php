@extends("layouts._pdf")
@section("content")
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th width="7%"><div class="text-center">N°</div></th>
        <th><div class="text-center">Nom abonné</div></th>
        <th><div class="text-center">Nom appelant</div></th>
        <th><div class="text-center">Date appel</div></th>
        <th><div class="text-center">Affectation</div></th>
        <th><div class="text-center">Type de Panne</div></th>
        <th><div class="text-center">Exploitation</div></th>
        <th><div class="text-center">Direction</div></th>
        <th><div class="text-center">Priorité</div></th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $reclamation)
        <tr>
            <td class="center">{{$reclamation->numero}}</td>
            <td class="center">{{$reclamation->nom." ".$reclamation->prenoms}}</td>
            <td class="center">{{$reclamation->nomappelant ? $reclamation->nomappelant : $reclamation->nom." ".$reclamation->prenoms}}</td>
            <td class="center">{{ (new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y') }}</td>
            <td class="center">{{$reclamation->statut}}</td>
            <td class="center">{{$reclamation->pannelib}}</td>
            <td class="center">{{$reclamation->exploitationlib}}</td>
            <td class="center">{{$reclamation->libelle}}</td>
            <td class="center">{{$reclamation->prioritelib}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection