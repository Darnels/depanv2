@extends('layouts._layout')

@section('link')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <form class="form-horizontal" method="get" action="">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4">Periode du : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodedebut" value="{{ request('periodedebut') ? request('periodedebut') : Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-1 col-sm-4 col-xs-4"> au : </label>
                                <div  class="col-md-2 col-sm-2 col-xs-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right calendar" name="periodefin" value="{{ request('periodefin') ? request('periodefin') : Carbon\Carbon::now()->endOfMonth()->format('d/m/Y') }}"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-2 col-sm-4 col-xs-4"> Quart : </label>
                                <div  class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <select name="q" class="form-control">
                                            @foreach($quart as $k => $q)
                                                <option value="{{$k}}" @if( ( request('q') ? request('q') : null ) == $k) selected @endif>{{$q}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-success" type="submit">Rechercher</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>Equipes </h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Equipe</th>
                                <th>Nombre total affectés</th>
                                <th>Nombre total dépanés</th>
                                <th>Nombre total non executés</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($equipes as $data)
                                <tr>
                                    <td>{{ $data->libelle }}</td>
                                    <td>{{ $data->NON_EXEC + $data->DEPAN }}</td>
                                    <td>{{ $data->DEPAN }}</td>
                                    <td>{{ $data->NON_EXEC }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>Zones </h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Zone</th>
                                <th>Nombre total affectés</th>
                                <th>Nombre total dépanés</th>
                                <th>Nombre total non executés</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($zones as $data)
                                <tr>
                                    <td>{{ $data->libelle }}</td>
                                    <td>{{ $data->NON_EXEC + $data->DEPAN }}</td>
                                    <td>{{ $data->DEPAN }}</td>
                                    <td>{{ $data->NON_EXEC }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>Secteurs </h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover table-bordered" id="exploitation">
                            <thead>
                            <tr>
                                <th>Secteur / Exploitation</th>
                                <th>Nombre total affectés</th>
                                <th>Nombre total dépanés</th>
                                <th>Nombre total non executés</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($secteurs as $data)
                                <tr>
                                    <td>{{ $data->libelle }}</td>
                                    <td>{{ $data->NON_EXEC + $data->DEPAN }}</td>
                                    <td>{{ $data->DEPAN }}</td>
                                    <td>{{ $data->NON_EXEC }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <!-- DataTables -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>

    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{request()->getBaseUrl()}}/plugins/datepicker/bootstrap-datepicker.js"></script>

    <script type="text/javascript" >

        $(function () {
            //Date picker
            $('.calendar').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'fr'
            });

            //DataTables
            $("#exploitation").DataTable();
        });
    </script>
@endsection