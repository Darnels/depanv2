
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.anme','DepanV2')}} | CIE </title>
    <link rel="icon" href="{{ asset("favicon.ico") }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset("/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/dist/css/AdminLTE.min.css") }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="{{ route("login") }}"><b>{{ config('app.name', 'Laravel') }}</b>CRC</a>
    </div>
    @if($errors->count() > 0)
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Erreur !</h4>
        @foreach($errors->all() as $error)
            <strong>{{ $error }}</strong><br/>
        @endforeach
    </div>
    @endif
    <!-- User name -->
    <div class="lockscreen-name">{{ $user->name }} <br> ({{ $user->email }})</div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="{{ asset("/profile/".$user->profile) }}" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="post">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="password" required class="form-control" name="password_old" placeholder="Mot de passe actuel">
                <input type="password" required class="form-control" name="password" placeholder="Nouveau mot de passe">
                <input type="password" required class="form-control" name="password_confirmation" placeholder="Confirmation mot de passe">

                <div class="input-group-btn">
                    <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
                <input type="hidden" name="email" value="{{ $user->email }}">
            </div>
        </form>
        <!-- /.lockscreen credentials -->

    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Modifier votre mott de passe
    </div>
    <div class="text-center">
        <a href="{{ route("login") }}">Se connecter avec un autre compte</a>
    </div>
    <div class="lockscreen-footer text-center">
        <strong>Copyright &copy; 2016 <a href="#">Djera-Services</a>.</strong> All rights reserved.
    </div>
</div>
<!-- /.center -->

<!-- jQuery 3 -->
<script src="{{ asset("pugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src=".{{ asset("bootstrap/js/bootstrap.js") }}"></script>
</body>
</html>