@extends('layouts._layout')

@section('content')
<div class=" col-md-offset-1 col-md-5 col-sm-6 col-xs-12">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-aqua-active">
            <h3 class="widget-user-username">{{ $user->name }}</h3>
            <h5 class="widget-user-desc">Web user</h5>
        </div>
        <div class="widget-user-image">
            <img class="img-circle" src="{{ request()->getBaseUrl() }}/profile/{{ $user->profile }}" alt="User Avatar">
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header">Bons créés ce mois</h5>
                        <span class="description-text">{{ $bonCrees }}</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                    @php(\Carbon\Carbon::setLocale('fr'))
                    <div class="description-block">
                        <h5 class="description-header">Actif </h5>
                        <span class="description-text">{{ (new \Carbon\Carbon($user->created_at))->diffForHumans() }}</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                    <div class="description-block">
                        <h5 class="description-header">Rôle</h5>
                        <span class="description-text">{{ $user->role ? implode(", ",array_keys($user->role)) : '' }}</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>

<div class=" col-md-5 col-sm-6 col-xs-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Modification de l'utilisateur</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                    <label for="">Nom et prénoms</label>
                    <input type="text" name="name" class="form-control"  placeholder="Saisir votre nom" value="{{ old('name',$user->name) }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" name="email_view" class="form-control" disabled placeholder="Entrer votre email" value="{{ old('email',$user->email) }}">
                    <input type="hidden" name="email" class="form-control" value="{{ old('email',$user->email) }}">
                </div>

                <hr/>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="changePassword" name="changePassword" > Changer de mot de passe
                        </label>
                    </div>
                </div>
                <div id="slide" style="display: none">
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" id="password" class="form-control"  placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirmation</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control"  placeholder="Confirmation du mot de passe">
                </div>
                </div>
                <hr/>

                <div class="form-group">
                    <label for="exampleInputFile">Photo</label>
                    <input type="file" id="profile" name="profile">

                    <p class="help-block">Veuillez choisir une photo de profil</p>
                </div>

                <hr/>
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) )
                @foreach($roles->toArray() as $role => $value)
                    <label>
                        <input type="checkbox" name="role[{{ $role }}]" value="1" @if($user->checkRole($role)) checked @endif> {{ $role }}
                    </label>
                @endforeach
                    <hr/>
                    <div class="form-group">
                        <label for="">Statut</label>
                        <select id="statut" name="statut" class="form-control">
                            @foreach(\App\Status::getUserStatusList()->toArray() as $key => $value)
                            <option @if(old('statut',$user->statut) == $value) selected @endif value="{{$value}}">{{ ucfirst($key) }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script type="application/javascript">
        $("#changePassword").click(function () {
            if($("#changePassword").is(':checked'))
                $("#slide").fadeIn();
            else
                $("#slide").fadeOut();
        });
    </script>
@endsection