@extends('layouts._layout')

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste des utilisateurs</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-add"></i> Nouvel utilisateur</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($roles as  $role=>$value )
        <div class="box box-success">
            <div class="box-header">
                <h3>{{ \App\Metier\Role::getProfileFullName($role) }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive ">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom et prénoms</th>
                        <th>Email</th>
                        <th>Droits</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    @if(array_key_exists($role, $user->role))
                    <tr>
                        <td>#{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role ? implode(", ",array_keys($user->role)) : '' }}</td>
                        <td>
                            <a class="label label-success" href="{{ route('user_profile',['email'=>base64_encode($user->email)]) }}"><i class="fa fa-edit"></i> Modifier</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @endforeach
        <!-- /.box -->

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Nouvel utilisateur</h4>
                    </div>
                    <div class="modal-body">
                        <!-- form start -->
                        <form role="form" method="post" action="" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="">Nom et prénoms</label>
                                    <input type="text" name="name" class="form-control"  placeholder="Saisir votre nom" value="{{ old('name') }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" name="email" class="form-control"  placeholder="Entrer votre  email" value="{{ old('email') }}">
                                </div>

                                <hr/>
                                <input type="hidden" value="on" id="changePassword" name="changePassword" >
                                <div id="slide">
                                    <div class="form-group">
                                        <label for="">Mot de passe</label>
                                        <input type="password" name="password" class="form-control"  placeholder="Mot de passe" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Confirmation</label>
                                        <input type="password" name="password_confirmation" class="form-control"  placeholder="Confirmation du mot de passe">
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">
                                    <label for="exampleInputFile">Photo</label>
                                    <input type="file" id="profile" name="profile">

                                    <p class="help-block">Veuillez choisir une photo de profil</p>
                                </div>

                                <hr/>
                                @foreach($roles->toArray() as $role => $value)
                                    <label>
                                        <input type="checkbox" name="role[{{ $role }}]" value="1"> {{ $role }}
                                    </label>
                                @endforeach
                                <hr/>
                                <div class="form-group">
                                    <label for="">Statut</label>
                                    <select id="statut" name="statut" class="form-control">
                                        @foreach(\App\Status::getUserStatusList()->toArray() as $key => $value)
                                            <option @if(old('statut') == $value) selected @endif value="{{$value}}">{{ ucfirst($key) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection