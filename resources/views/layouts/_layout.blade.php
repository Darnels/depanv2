<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.anme','DepanV2')}} | CIE </title>
    <link rel="icon" href="{{request()->getBaseUrl()}}/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset("/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Dynamic Theme style -->
    @yield('link')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/dist/css/AdminLTE.min.css") }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{ asset("/dist/css/skins/skin-cie.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/custom.css") }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('accueil')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>C</b>RC</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>{{ config('app.name', 'Laravel') }}</b> - CRC</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                <!--
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="{{request()->getBaseUrl()}}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">Voir tous les messages</a></li>
                        </ul>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Vous avez 10 notifications</li>
                            <li>
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 9 tasks</li>
                            <li>
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">Voir toutes les taches</a>
                            </li>
                        </ul>
                    </li>
                    -->
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="{{route('logout')}}" class="dropdown-toggle btn btn-danger" > <!-- data-toggle="dropdown" -->
                            <!-- The user image in the navbar-->
                            Déconnexion
                        </a>
                   {{--     <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                           --}}{{-- <li class="user-header">
                                <img src="{{request()->getBaseUrl()}}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            </li>--}}{{--
                            <!-- Menu Body --
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">Déconnexion</a>
                                </div>
                            </li>
                        </ul>--}}
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                   <img src="{{request()->getBaseUrl()}}/profile/{{ \Illuminate\Support\Facades\Auth::user()->profile }} " class = "img-circle" alt = "User Image">
                </div>
                <div class="pull-left info">
                    <p><a href="{{route('user_profile',['email'=>base64_encode(\Illuminate\Support\Facades\Auth::user()->email)])}}">{{\Illuminate\Support\Facades\Auth::user()->name}}</a></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="{{route('recherche_client')}}" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="query" class="form-control" placeholder="Recherche...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MENU</li>
                <!-- Optionally, you can add icons to the links -->
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::AGENT_CRC) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::CHARGE_CRC) )
                <li class="treeview">
                    <a href="#"><i class="fa fa-file-o"></i> <span>CREATION DE BONS</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('recherche_client')}}">Recherche Client</a></li>
                    </ul>
                </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::AGENT_CRC) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::RESPONSABLE_UODA) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::CHARGE_CRC) )
                <li class="treeview">
                    <a href="#"><i class="fa fa-files-o"></i> <span>MODIFICATION /</br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONSULTATION DE BONS</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('reclamation_nonaffectees')}}">En attente d'affectation <span class="pull-right-container"><small class="label pull-right bg-yellow">{{ \App\Reclamation::where("status", \App\Status::RECLAMATION_CREEE)->count() }}</small></span> </a></li>
                        <li><a href="{{route('reclamation_encours')}}">En attente de traitement <span class="pull-right-container"><small class="label pull-right bg-yellow">{{ \App\Reclamation::where("status", \App\Status::RECLAMATION_TRANSFEREE_EQUIPE)->count() }}</small></span> </a></li>
                        <li><a href="{{route('reclamation_terminees')}}">Clôturées <span class="pull-right-container"><small class="label pull-right bg-yellow">{{ \App\Reclamation::where("status", \App\Status::RECLAMATION_TRAITEE_TERMINE)->count() }}</small></span> </a></li>
                    </ul>
                </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::CHARGE_CRC) )
                <li class="treeview">
                    <a href="#"> <i class="fa fa-bar-chart-o"></i> <span>RAPPORTS CRC</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('rapport_mensuel')}}"><span>Etats Mensuels</span></a></li>
                        <li class="active"><a href="{{route('rapport_appel')}}"><span>Liste des Appels</span></a></li>
                    </ul>
                </li>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::RESPONSABLE_UODA) )
                <li class="treeview">
                    <a href="{{route('stat_uoda')}}"><i class="fa fa-pie-chart"></i> <span>RAPPORTS UODA</span></a>
                </li>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::RESPONSABLE_UODA) ||
                \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::CHARGE_CRC) )
                <li class="treeview">
                    <a href="#"> <i class="fa fa-bar-chart-o"></i> <span>POST TRANSFERT </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEPANNAGE</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('rapport_pro')}}"><span>Liste des Reclamations PRO</span></a></li>
                        <li class="active"><a href="{{route('rapport_mai')}}"><span>Liste des Reclamations MAI</span></a></li>
                        <li class="active"><a href="{{route('rapport_def')}}"><span>Liste des Reclamations DEF</span></a></li>
                    </ul>
                </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) )
                <li class="treeview">
                    <a href="#"><i class="fa fa-suitcase"></i> <span>ADMINISTRATION</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('carte')}}"><span>Cartographie</span></a></li>
                        <li class=""><a href="{{route('user_liste')}}"><span>Utilisateurs</span></a></li>
                        <li class=""><a href="{{route('equipe_liste')}}"><span>Equipes</span></a></li>
                        <li class=""><a href="{{route("liste_appareils")}}"><span>Appareils</span></a></li>
                    </ul>
                </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) )
                <li class="treeview">
                    <a href="#"><i class="fa fa-tachometer"></i> <span>TABLEAU DE BORD</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route("liste_appareils")}}"><span>Equipes</span></a></li>
                    </ul>
                </li>
                @endif
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @foreach($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Erreur !</h4>
                    {{$message}}
                </div>
            @endforeach
            @if(session()->has(\App\Notify\Notification::NOTIFICATION_KEY))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Succès !</h4>
                {{session(\App\Notify\Notification::NOTIFICATION_KEY)}}
            </div>
            @endif
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    @yield('content') <!-- Your Page Content Here -->
    </div>
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class="main-footer">
<!-- To the right -->
<div class="pull-right hidden-xs">
CIE
</div>
<!-- Default to the left -->
<strong>Copyright &copy; 2016 <a href="#">Djera-Services</a>.</strong> All rights reserved.
</footer>

<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{request()->getBaseUrl()}}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{request()->getBaseUrl()}}/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="{{request()->getBaseUrl()}}/dist/js/app.min.js"></script>

<!-- Dynamics scripts -->
@yield('script')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. Slimscroll is required when using the
fixed layout. -->
</body>
</html>