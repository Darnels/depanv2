<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.url') }}</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/pdf.css')}}" media="all" />
    <style>
        .page{
            page-break-after: auto;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div>
        <div id="logo">
            <a href="{{ config('app.url') }}"><img id="cie" src="{{ asset('logo-cie.jpg') }}"/></a>
            <a href="{{ config('app.url') }}"><img id="depan" src="{{ asset('ic_launcher.jpg') }}"/></a>
        </div>
    </div>
</header>

<hr/>

<h3 id="title">Liste des bons provisoires</h3>
<br/>
<br/>
<main class="page">
    @yield('content')
</main>

<footer>
    <p>{{ config("app.name")  }} &copy; {{ date('Y') }}</p>
</footer>
</body>
</html>