@extends('layouts._layout')

@section('link')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{request()->getBaseUrl()}}/plugins/select2/select2.min.css">
@endsection

@section('content')
    <div class=" col-md-8 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Nouvelle réclamation</h3>
            </div>
            <form role="form" class="form-horizontal form-label-left" method="post" action="">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Référence client</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input name="clientRef" type="text" class="form-control" placeholder="Référence du client" value="{{$client->refbranch}}" disabled>
                            <input type="hidden" name="client_id" value="{{$client->id}}">
                        </div>

                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Identifiant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Référence du client" value="{{$client->idabon}}" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        @if(! $client->longitude || $client->longitude == 0)
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 col-xs-12">
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-warning"></i> Avertissement !</h4>
                                    Les coordonnées GPS de ce client sont intouvables
                                </div>
                            </div>
                        @endif
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">GPS Latitude</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Latitude" name="latitude" id="latitude" value="{{old('latitude') ? old('latitude') : $client->latitude }}" >
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">GPS Longitude</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Longitude"  name="longitude" id="longitude" value="{{old('longitude') ? old('longitude') : $client->longitude }}" >
                        </div>

                        <input type="hidden" name="updategps" value="0" id="updategps">

                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Raison sociale</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="M, Mme, Mlle, Entreprise" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="control-label col-md-2 col-sm-2 col-xs-12">Nom</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Nom du client" value="{{$client->nom}}" disabled>
                        </div>

                        <label  class="control-label col-md-2 col-sm-2 col-xs-12">Prénoms</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Prénoms du client" value="{{$client->prenoms}}" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">N° compteur</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Téléphone 1" value="{{$client->numctr}}" disabled>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Téléphone 2</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Téléphone 2" value="{{$client->phoneTo}}" disabled>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="box-header with-border">
                    <h3 class="box-title"> Infos de l'interlocuteur </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12"> L'interlocuteur n'est pas abonné </label>
                        <div class="col-md-3 col-sm-2 col-xs-12">
                            <input type="checkbox" id="nonabonne" name="nonabonne" value="0">
                        </div>
                    </div>
                    <div class="form-group" id="detail">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Nom de l'appelant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Nom interlocuteur" value="{{old('nomappelant')}}" name="nomappelant">
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Contact appelant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Contact interlocuteur" value="{{old('contactappelant')}}" name="contactappelant">
                        </div>
                    </div>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title"> Panne infos </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Priorité</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="priorite_id">
                                @foreach($priorites as $priorite)
                                    <option value="{{$priorite->id}}" @if(old('priorite_id') == $priorite->id) selected @endif> {{$priorite->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Sensibilité</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="sensibilite_id">
                                @foreach($sensibilites as $sensibilite)
                                    <option value="{{$sensibilite->id}}" @if(old('sensibilite_id') == $sensibilite->id) selected @endif> {{$sensibilite->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Panne signalée</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="panne_id">
                                @foreach($pannes as $panne)
                                <option value="{{$panne->id}}" @if(old('panne_id') == $panne->id) selected @endif> {{$panne->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Exploitation</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2" style="width: 100%;" name="exploitation_id">
                                @foreach($exploitations as $exploitation)
                                <option value="{{$exploitation->id}}" @if(substr($client->refbranch,0,3) == $exploitation->code) selected @endif>{{$exploitation->code}} {{$exploitation->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Canal de communication</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="canalcommunication_id">
                                @foreach($canalCommunication as $canal)
                                    <option value="{{$canal->id}}" @if(old('canalcommunication_id') == $canal->id) selected @endif> {{$canal->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Type de réclamation</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="typereclamation_id">
                                @foreach($typeReclamation as $type)
                                    <option value="{{$type->id}}" @if(old('typereclamation_id') == $type->id) selected @endif> {{$type->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Adresse géographique</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" placeholder="Commune, quartier, sous-quartier, espace type, ..." name="localisation" rows="3" >{{old('localisation')}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Commentaire</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" placeholder="Commentaire ..." name="commentaire" rows="3" >{{old('commentaire')}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-sm-offset-2 col-md-3 col-sm-2 col-xs-12">
                        <label for="depanagenligne" class=""> Dépannage en ligne
                            <input type="checkbox" id="depanageenligne" name="depanageenligne" class="flat-red" value="1" @if(old('depanagenligne') == 1)checked @endif>
                        </label>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 col-xs-12" id="parentMap">
        <div id="map" style="height: 800px"></div>
        <div style="margin-top: 10px">
            <input style="width: 300px; top: 8px;" type="text" class="form-control" value="" placeholder="Localisation zoom" id="google-search">
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste des réclamations du même client</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Référence ID</th>
                        <th>Date</th>
                        <th>Statut</th>
                        <th>Localisation</th>
                    </tr>
                    @if($oldReclamations)
                        @foreach($oldReclamations as $reclamation)
                            <tr>
                                <td>{{$reclamation->numero}}</td>
                                <td>{{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y')}}</td>
                                <td><span class="label label-default">@lang("message.status.".$reclamation->status)</span></td>
                                <td>{{$reclamation->localisation}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4"><h3>Ce client n'a pas d'anciennes réclamations.</h3></td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- Select2 -->
<script src="{{request()->getBaseUrl()}}/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });

    var latitude = document.getElementById("latitude");
    var longitude = document.getElementById("longitude");
    var updategps = document.getElementById("updategps");

    var map = null;
    var marker = null;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 5.330306, lng: -3.9763826}, //Abidjan
            zoom: 12
        });

        //Place ID et zoom
        var inputSearch = document.getElementById("google-search");
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputSearch);

        //Set auto complete
        var options = {
            componentRestrictions: {country: ['ci']}
        };
        var autocomplete = new google.maps.places.Autocomplete(inputSearch, options);

        setAutocomplete(map, autocomplete);

        var markerClient = new google.maps.Marker({
            position: {lat:parseFloat('{{$client->latitude}}'),lng:parseFloat('{{$client->longitude}}')},
            map: map,
            title: 'Client {{$client->nom." ".$client->prenoms}}',
            icon :'{{request()->getBaseUrl()}}/Bonhomme30.png'
        });

        map.addListener('click',function(e){
            console.log(e);
            latitude.setAttribute("value",e.latLng.lat());
            longitude.setAttribute("value",e.latLng.lng());
            updategps.setAttribute("value",1);

            if(marker)
                marker.setMap(null);

            marker = new google.maps.Marker({
                map: map,
                title: "Localisation de la panne du client",
                animation: google.maps.Animation.DROP,
                position: e.latLng,
                icon :'{{request()->getBaseUrl()}}/panne.png'
            });
        });
    }

    $('#nonabonne').click(function () {
        if($(this).prop("checked"))
            $("#detail").show();
        else
            $("#detail").hide();
    });

    function setAutocomplete(map, autocomplete) {

        autocomplete.bindTo('bounds', map);

        autocomplete.addListener('place_changed', function() {

            var place = autocomplete.getPlace();

            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
        });
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&libraries=places&callback=initMap"
        async defer>
</script>
@endsection