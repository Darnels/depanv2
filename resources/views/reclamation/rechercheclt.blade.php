@extends('layouts._layout')

@section('link')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{request()->getBaseUrl()}}/plugins/select2/select2.min.css">
@endsection

@section('content')
    <div class=" col-md-8 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Recherche d'abonné pour la Création de Bons</h3>
            </div>
            <form action="" method="post" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="query" class="form-control" placeholder="Recherche...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>

@endsection