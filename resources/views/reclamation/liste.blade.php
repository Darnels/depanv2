@extends('layouts._layout')

@section('content')
@if($reclamations)
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Liste des réclamations {{$type}}</h3>

            <div class="box-tools">

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody><tr>
                    <th>Reclamation ID</th>
                    <th>Client</th>
                    <th>Date de création</th>
                    <th>Date d'affectation</th>
                    <th>Date de cloture</th>
                    <th>Statut</th>
                    <th>Equipe en charge</th>
                    <th>Actions</th>
                </tr>
                @foreach($reclamations as $reclamation)
                <tr>
                    <td>{{$reclamation->numero}}</td>
                    <td>{{$reclamation->client->nom}} {{$reclamation->client->prenoms}}</td>
                    <td>{{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y à H:i:s')}}</td>
                    <td>{{$reclamation->dateaffectation ? (new \Carbon\Carbon($reclamation->dateaffectation))->format('d/m/Y à H:i:s') : 'Non affecté'}}</td>
                    <td>{{$reclamation->datefinintervention ? (new \Carbon\Carbon($reclamation->datefinintervention))->format('d/m/Y à H:i:s') : 'Non cloturé'}}</td>
                    <td>{{\App\Status::getString($reclamation->status)}}</td>
                    <td>@if($reclamation->depanageenligne) Dépanner en ligne @else{{$reclamation->equipe ? $reclamation->equipe->libelle : ''}}@endif</td>
                    <td>
                        @if( (!$reclamation->dateaffectation && $reclamation->depannageenligne) || (!$reclamation->dateaffectation && $reclamation->status == \App\Status::RECLAMATION_CREEE) )
                            <a href="{{route('plannification_bt',['reclamationNumber' => substr($reclamation->numero,strpos($reclamation->numero,\App\Http\Controllers\ReclamationController::PREFIX_RECLAMATION,0))])}}" class="label label-warning">
                                <i class="fa fa-send"></i> Affecter
                            </a>
                        @endif
                        <a href="{{route('reclamation_details',["reclamationNumber" => substr($reclamation->numero,strpos($reclamation->numero,\App\Http\Controllers\ReclamationController::PREFIX_RECLAMATION,0))])}}" class="label label-success"> <i class="fa fa-eye"></i> Consulter</a>

                        @if($reclamation->status != \App\Status::RECLAMATION_TRAITEE_TERMINE && (\Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::ADMINISTRATEUR) || \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::CHARGE_CRC) || \Illuminate\Support\Facades\Auth::user()->checkRole(\App\Metier\Role::AGENT_CRC)) )
                        <a href="{{route('up_reclamation',["reclamationNumber" => substr($reclamation->numero,strpos($reclamation->numero,\App\Http\Controllers\ReclamationController::PREFIX_RECLAMATION,0))])}}" class="label label-success"> <i class="fa fa-edit"></i> Modifier</a>
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{$reclamations->links()}}
    </div>
</div>
@else
    <div class="callout callout-info">
        <h4>Liste des réclamations!</h4>
        <p>Aucune réclamation trouvée</p>
    </div>
@endif
@endsection