@extends('layouts._layout')

@section('link')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{request()->getBaseUrl()}}/plugins/select2/select2.min.css">
@endsection

@section('content')

    <div class="box box-primary col-md-12 col-sm-12 col-xs-12">
        <div class="box-header">
            <div class="col-md-2">
                <a href="{{route('new_reclamation',['referenceClient' => $client->idabon])}}" class=" btn btn-block btn-primary" title="Opuvelle réclamation">
                    <i class="fa fa-folder-open-o"></i> Nouvelle reclamation
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste des réclamations du même client</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Référence Réclamation</th>
                        <th>Nom Client</th>
                        <th>Type panne</th>
                        <th>Date</th>
                        <th>Statut</th>
                        <th>Localisation</th>
                        <th>Action</th>
                    </tr>
                    @if($oldReclamations->count())
                        @foreach($oldReclamations as $reclamation)
                            <tr>
                                <td>{{$reclamation->numero}}</td>
                                <td>{{$reclamation->client->nom}} {{$reclamation->client->prenoms}}</td>
                                <td>{{$reclamation->panne->libelle}}</td>
                                <td>{{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y')}}</td>
                                <td><span class="label label-default">@lang("message.status.".$reclamation->status)</span></td>
                                <td>{{$reclamation->localisation}}</td>
                                <td><a href="{{route('up_reclamation',["reclamationNumber" => substr($reclamation->numero,strpos($reclamation->numero,\App\Http\Controllers\ReclamationController::PREFIX_RECLAMATION,0))])}}" class="label label-success"> <i class="fa fa-edit"></i> Modifier</a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12" align="center">
                                <div class="jumbotron">
                                    <h2>Ce client n'a pas d'anciennes réclamations!</h2>
                                </div>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- Select2 -->
<script src="{{request()->getBaseUrl()}}/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });

    var latitude = document.getElementById("latitude");
    var longitude = document.getElementById("longitude");
    var updategps = document.getElementById("updategps");

    var map = null;
    var marker = null;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 5.330306, lng: -3.9763826}, //Abidjan
            zoom: 12
        });

        map.addListener('click',function(e){
            console.log(e);
            latitude.setAttribute("value",e.latLng.lat());
            longitude.setAttribute("value",e.latLng.lng());
            updategps.setAttribute("value",1);

            if(marker)
                marker.setMap(null);

            marker = new google.maps.Marker({
                map: map,
                title: "Localisation de la panne du client",
                animation: google.maps.Animation.DROP,
                position: e.latLng,
                icon :'{{request()->getBaseUrl()}}/panne.png'
            });
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&callback=initMap"
        async defer>
</script>
@endsection