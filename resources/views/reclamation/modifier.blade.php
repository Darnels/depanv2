@extends('layouts._layout')

@section('link')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{request()->getBaseUrl()}}/plugins/select2/select2.min.css">
@endsection

@section('content')
    <div class=" col-md-8 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> réclamation</h3>
            </div>
            <form role="form" class="form-horizontal form-label-left" method="post" action="">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Référence client</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input name="clientRef" type="text" class="form-control" placeholder="Référence du client" value="{{$reclamation->client->refbranch}}" disabled>
                            <input type="hidden" name="client_id" value="{{$reclamation->client->id}}">
                            <input type="hidden" name="numero" value="{{$reclamation->numero}}">
                        </div>

                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Identifiant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Référence du client" value="{{$reclamation->client->idabon}}" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        @if(! $reclamation->client->longitude || $reclamation->client->longitude == 0)
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 col-xs-12">
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-warning"></i> Avertissement !</h4>
                                    Les coordonnées GPS de ce client sont intouvables
                                </div>
                            </div>
                        @endif
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">GPS Latitude</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Latitude" name="latitude" id="latitude" value="{{old('latitude') ? old('latitude') : $reclamation->client->latitude }}" >
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">GPS Longitude</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" placeholder="Longitude"  name="longitude" id="longitude" value="{{old('longitude') ? old('longitude') : $reclamation->client->longitude }}" >
                        </div>

                        <input type="hidden" name="updategps" value="0" id="updategps">

                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Raison sociale</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="M, Mme, Mlle, Entreprise" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="control-label col-md-2 col-sm-2 col-xs-12">Nom</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Nom du client" value="{{$reclamation->client->nom}}" disabled>
                        </div>

                        <label  class="control-label col-md-2 col-sm-2 col-xs-12">Prénoms</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Prénoms du client" value="{{$reclamation->client->prenoms}}" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">N° compteur</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Téléphone 1" value="{{$reclamation->client->numctr}}" disabled>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Téléphone 2</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Téléphone 2" value="{{$reclamation->client->phoneTo}}" disabled>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="box-header with-border">
                    <h3 class="box-title"> Infos de l'interlocuteur </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12"> L'interlocuteur n'est pas abonné </label>
                        <div class="col-md-3 col-sm-2 col-xs-12">
                            <input type="checkbox" id="nonabonne" name="nonabonne" value="1" @if(old('nonabonne')?old('nonabonne'):$reclamation->nonabonne) checked @endif >
                        </div>
                    </div>
                    <div class="form-group" id="detail">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Nom de l'appelant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Nom interlocuteur" value="{{(old('nomappelant')?old('nomappelant'):$reclamation->nomappelant)}}" name="nomappelant">
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Contact appelant</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" placeholder="Contact interlocuteur" value="{{(old('contactappelant')?old('contactappelant'):$reclamation->contactappelant)}}" name="contactappelant">
                        </div>
                    </div>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title"> Panne infos </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Priorité</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="priorite_id">
                                @foreach($priorites as $priorite)
                                    <option value="{{$priorite->id}}" @if((old('priorite_id')? old('priorite_id'):$reclamation->priorite->id) == $priorite->id) selected @endif> {{$priorite->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Sensibilité</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="sensibilite_id">
                                @foreach($sensibilites as $sensibilite)
                                    <option value="{{$sensibilite->id}}" @if((old('sensibilite_id')? old('sensibilite_id'):$reclamation->sensibilite->id) == $sensibilite->id) selected @endif> {{$sensibilite->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Panne signalée</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="panne_id">
                                @foreach($pannes as $panne)
                                    <option value="{{$panne->id}}" @if((old('panne_id')?old('panne_id'):$reclamation->panne->id) == $panne->id) selected @endif> {{$panne->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Exploitation</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2" style="width: 100%;" name="exploitation_id">
                                @foreach($exploitations as $exploitation)
                                    <option value="{{$exploitation->id}}" @if(substr($reclamation->client->refbranch,0,3) == $exploitation->code) selected @endif>{{$exploitation->code}} {{$exploitation->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Canal de communication</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="canalcommunication_id">
                                @foreach($canalCommunication as $canal)
                                    <option value="{{$canal->id}}" @if((old('canalcommunication_id')?old('canalcommunication_id'):$reclamation->canalCommunnication->id) == $canal->id) selected @endif> {{$canal->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Type de réclamation</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control select2 col-md-12" style="width: 100%;" name="typereclamation_id">
                                @foreach($typeReclamation as $type)
                                    <option value="{{$type->id}}" @if((old('typereclamation_id')?old('typereclamation_id'):$reclamation->type->id) == $type->id) selected @endif> {{$type->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Adresse géographique</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" placeholder="Commune, quartier, sous-quartier, espace type, ..." name="localisation" rows="3" >{{(old('localisation')?old('localisation'):$reclamation->localisation)}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Commentaire</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" placeholder="Commentaire ..." name="commentaire" rows="3" >{{(old('commentaire')?old('commentaire'):$reclamation->commentaire)}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-sm-offset-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="depanagenligne" class=""> Dépannage en ligne
                                <input type="checkbox" id="depanageenligne" name="depanageenligne" class="flat-red" value="1" @if(old('depanagenligne')?old('depanagenligne'):$reclamation->depanagenligne == 1)checked @endif>
                            </label>
                            <div class="col-md-offset-1 col-sm-offset-1 col-md-6 col-sm-6 col-xs-12">
                                <label for="relance" class=""> Relance du bon
                                    <input type="checkbox" id="relance" name="relance" class="flat-red" value="1" @if(old('relance')?old('relance'):$reclamation->relance == 1)checked @endif>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 col-xs-12">
        <div id="map" style="height: 800px"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste des réclamations du même client</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Référence ID</th>
                        <th>Date</th>
                        <th>Statut</th>
                        <th>Localisation</th>
                    </tr>

                    @if($oldReclamations)
                        @foreach($oldReclamations as $reclamation)
                            <tr>
                                <td>{{$reclamation->numero}}</td>
                                <td>{{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y')}}</td>
                                <td><span class="label label-default">@lang("message.status.".$reclamation->status)</span></td>
                                <td>{{$reclamation->localisation}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4"><h3>Ce client n'a pas d'anciennes réclamations.</h3></td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Select2 -->
    <script src="{{request()->getBaseUrl()}}/plugins/select2/select2.full.min.js"></script>

    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });

        var latitude = document.getElementById("latitude");
        var longitude = document.getElementById("longitude");
        var updategps = document.getElementById("updategps");

        var map = null;
        var marker = null;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 5.330306, lng: -3.9763826}, //Abidjan
                zoom: 12
            });

            var markerClient = new google.maps.Marker({
                position: {lat:parseFloat('{{$reclamation->client->latitude}}'),lng:parseFloat('{{$reclamation->client->longitude}}')},
                map: map,
                title: 'Client {{$reclamation->client->nom." ".$reclamation->client->prenoms}}',
                icon :'{{request()->getBaseUrl()}}/Bonhomme30.png'
            });

            map.addListener('click',function(e){
                console.log(e);
                latitude.setAttribute("value",e.latLng.lat());
                longitude.setAttribute("value",e.latLng.lng());
                updategps.setAttribute("value",1);

                if(marker)
                    marker.setMap(null);

                marker = new google.maps.Marker({
                    map: map,
                    title: "Localisation de la panne du client",
                    animation: google.maps.Animation.DROP,
                    position: e.latLng,
                    icon :'{{request()->getBaseUrl()}}/panne.png'
                });
            });
        }

        $('#nonabonne').click(function () {
            if($(this).prop("checked"))
                $("#detail").show();
            else
                $("#detail").hide();
        });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&callback=initMap"
            async defer>
    </script>
@endsection