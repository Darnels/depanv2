@extends('layouts._layout')

@php
    \Carbon\Carbon::setLocale('fr');
@endphp

@section('content')
<section class="col-md-12 col-sm-12 col-xs-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <i class="fa fa-gears"></i> Dépannage N° {{$reclamation->numero}}
            @if($reclamation->status == \App\Status::RECLAMATION_TRANSFEREE_EQUIPE)
            <a href="{{ route("plannification_bt",['reclamationNumber'=>$reclamation->numero,'reaffect'=>"true"]) }}" class="btn btn-sm btn-success">Reaffecter</a>
            @endif
            <small class="pull-right">Créé le : {{(new \Carbon\Carbon($reclamation->datereclamation))->format('d/m/Y à H:i:s')}}</small>
        </div>

        <div class="box-body">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h4>Abonné</h4>
                <address>
                    <strong>{{$reclamation->client->nom}} {{$reclamation->client->prenoms}}</strong><br>
                    @if($reclamation->nonabonne)<b>Interlocuteur :</b> {{$reclamation->nomappelant}} @endif<br>
                    <b>Référence client :</b> {{$reclamation->client->refbranch}}<br>
                    <b>Identifiant client :</b> {{$reclamation->client->idabon}}<br>
                    <b>Contact:</b> @if($reclamation->nonabonne)(Contact interlocuteur : {{$reclamation->contactappelant}}) @endif
                </address>
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h4>Infos panne</h4>
                <address>
                    <strong>{{$reclamation->priorite->libelle}}</strong><br>
                    <strong>{{$reclamation->sensibilite->libelle}}</strong><br>
                    {{$reclamation->panne->libelle}}<br>
                    {{$reclamation->canalCommunnication->libelle}}<br>
                    {{$reclamation->exploitation->code}} - {{$reclamation->exploitation->libelle}}<br>
                    {{$reclamation->type->libelle}}
                </address>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <h4>Enchainement</h4>
                Statut : <b> {{\App\Status::getString($reclamation->status)}} : {{$reclamation->equipe ? $reclamation->equipe->libelle : 'Non affecté'}}</b> <br/>
                @if($reclamation->datedemarragenavigation) Démarrage de la navigation : <b>{{(new \Carbon\Carbon($reclamation->datedemarragenavigation))->format('d/m/Y à H:i:s')}}</b> <small>{{(new \Carbon\Carbon($reclamation->datedemarragenavigation))->diffForHumans(new \Carbon\Carbon($reclamation->dateaffectation))}}</small><br> @endif
                @if($reclamation->datedebutintervention) Fin de la navigation : <b>{{(new \Carbon\Carbon($reclamation->datedebutintervention))->format('d/m/Y à H:i:s')}} </b> <small>{{(new \Carbon\Carbon($reclamation->datedemarragenavigation))->diffForHumans(new \Carbon\Carbon($reclamation->datedebutintervention))}}</small><br> @endif
                @if($reclamation->datedebutintervention) Début d'intervention : <b>{{(new \Carbon\Carbon($reclamation->datedebutintervention))->format('d/m/Y à H:i:s')}} </b> <small>{{(new \Carbon\Carbon($reclamation->datedemarragenavigation))->diffForHumans(new \Carbon\Carbon($reclamation->datedebutintervention))}}</small><br> @endif
                @if($reclamation->datefinintervention) Fin d'intervention <b>{{(new \Carbon\Carbon($reclamation->datefinintervention))->format('d/m/Y à H:i:s')}} </b> <small>{{(new \Carbon\Carbon($reclamation->datefinintervention))->diffForHumans(new \Carbon\Carbon($reclamation->datedebutintervention))}}</small><br> @endif
            </div>

            <hr/>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>
                        <h3 class="box-title">Rapport de l'équipe</h3>
                    </div>

                    <div class="box-body">
                        <blockquote>
                            <p>{{$reclamation->compterendu ? $reclamation->compterendu->detailrapport: ""}}</p>
                        </blockquote>
                    </div>
                </div>
                @if($reclamation->rapportcrc)
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>
                        <h3 class="box-title">Rapport de l'agent CRC</h3>
                    </div>

                    <div class="box-body">
                        <blockquote>
                            <p>{{$reclamation->rapportcrc}}</p>
                        </blockquote>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="box-footer">
            <div class="box clearfix">
                <form method="post" action="">
                    {{csrf_field()}}
                    <div class="box-header">
                        <div class="box-title">Rapport du CRC</div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea name="rapportcrc" rows="3" class="form-control" placeholder="Veuillez saisir le rapport du CRC"></textarea>
                                <input type="hidden" name="numero" value="{{$reclamation->numero}}">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Enregistrer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection