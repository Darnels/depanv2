@extends('layouts._layout')
@php
\Carbon\Carbon::setLocale('fr');
@endphp
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @foreach($reclamations as $reclamation)
        <div class="box box-widget collapsed-box box-solid">
            <div class="box-header with-border">
                <div class="user-block">
                    <img class="img-circle" src="{{request()->getBaseUrl()}}/ic_launcher.jpg" alt="User Image">
                    <span class="username"><a href="#">Réclamation {{$reclamation->numero}}</a></span>
                    <span class="description">{{(new \Carbon\Carbon($reclamation->dateaffectation ? $reclamation->dateaffectation : $reclamation->datereclamation))->diffForHumans()}}</span>
                </div>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <dl class="dl-horizontal">
                        <dt>Client :</dt>
                        <dd>{{$reclamation->client->nom}} {{$reclamation->client->prenoms}}</dd>
                        <dt>Panne signalée :</dt>
                        <dd>la panne signalée est {{$reclamation->panne->libelle}}</dd>
                        <dt>Canal communication :</dt>
                        <dd>{{$reclamation->canalCommunnication->libelle}}</dd>
                        <dt>Type de maintenance :</dt>
                        <dd>{{$reclamation->type->libelle}}</dd>
                    </dl>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-6">
                    <iframe src="https://www.google.com/maps/embed/v1/place?key={{\App\Http\Controllers\MapController::MAP_EMBED_KEY}}&q={{$reclamation->client->latitude}},{{$reclamation->client->longitude}}" frameborder="0" style="border:0" allowfullscreen class="col-md-12 col-sm-12 col-xs-12" height="300"></iframe>
                </div>

            </div>
            <div class="box-footer clearfix">

            </div>
        </div>
        @endforeach
    </div>
@endsection

@section('script')
@endsection
