@extends('layouts._layout')

@php
    \Carbon\Carbon::setLocale('fr');
@endphp

@section('content')
    @if(request()->has("success"))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Succès !</h4>
            {{request()->session()->get("success")}}
        </div>
    @endif
<div class="col-md-3">
    <div class="box box-primary">
        <form class="form-horizontal" method="post" action="" enctype="application/x-www-form-urlencoded">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="{{request()->getBaseUrl()}}/saumsung_tab.jpg" alt="Tablette profile picture">
            <h3 class="profile-username text-center">{{$appareil->libelle}}</h3>
            <p class="text-muted text-center">{{$appareil->android_id}}</p>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Equipe connectée</b> <a class="pull-right">{{$appareil->equipe ? $appareil->equipe->libelle : null}}</a>
                </li>
                <li class="list-group-item">
                    <b>Date de connexion</b> @if($appareil->equipe) <a class="pull-right">{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$appareil->dateconnexion)->format("d/m/Y H:i:s")}}</a> @endif
                </li>
                <li class="list-group-item">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$appareil->id}}" />
                    <input type="text" name="libelle" class="form-control" value="{{$appareil->libelle}}"/>
                </li>
            </ul>

            <button type="submit" class="btn btn-primary btn-block"><b>Modifier</b></button>
        </div>
        </form>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Dernière position</h3>
        </div>
        <div class="box-body">
            @if($latestPosition)
                <iframe src="https://www.google.com/maps/embed/v1/place?key={{\App\Http\Controllers\MapController::MAP_EMBED_KEY}}&q={{$latestPosition->latitude}},{{$latestPosition->longitude}}" frameborder="0" style="border:0" allowfullscreen class="col-md-12 col-sm-12 col-xs-12" height="300"></iframe>
            @else
                <h3>Aucune position trouvée</h3>
            @endif
        </div>
    </div>
</div>
@endsection