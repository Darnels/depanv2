@extends('layouts._layout')
@section('content')
    @foreach($appareils as $appareil)
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="{{request()->getBaseUrl()}}/saumsung_tab.jpg" alt="User profile picture">

                <h3 class="profile-username text-center">{{$appareil->libelle}}</h3>

                <p class="text-muted text-center">{{$appareil->android_id}}</p>

                <a href="{{route('appareil_profil',["androidID" => $appareil->android_id])}}" class="btn btn-primary btn-block"><b>Voir plus ...</b></a>
            </div>
        </div>
    </div>
    @endforeach
@endsection