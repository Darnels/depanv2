@extends('layouts._layout')


@section('content')
<div class="col-xs-12">
    <div class="box">
        <div class="box-header box-primary">
            <h3 class="box-title">Liste des clients Par Critères</h3>
        </div>
        <div class="box-body">
            <div style="display: none" id="template">
                <div id="zone">
                    <div class="col-md-5">
                        <select name="critere[]" class="form-control">
                            @foreach($criteres as $critere => $name)
                                <option value="{{$critere}}">{{$name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="valeur[]" placeholder="valeur" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="remove"><i class="glyphicon glyphicon-trash"></i> </button>
                    </div>
                </div>
            </div>
            <form method="get" action="">
                <div class="form-group row" >
                    <div id="enveloppe" class="col-md-6">
                    <!-- Zone de travail -->
                        @if(request()->query("critere"))
                            <!-- Au cas où des critères ont été définis -->
                            @foreach( request()->query("critere") as $index => $value )
                            <div id="zone">
                                <div class="col-md-5">
                                    <select name="critere[]" class="form-control">
                                        @foreach($criteres as $critere => $name)
                                            <option value="{{$critere}}" @if($value == $critere) selected @endif >{{$name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="valeur[]" placeholder="valeur" class="form-control" value="{{ request()->query("valeur")[$index] }}">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="remove"><i class="glyphicon glyphicon-trash"></i> </button>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <!-- Au cas où aucun critère n'a été défini -->
                            <div id="zone">
                                <div class="col-md-5">
                                    <select name="critere[]" class="form-control">
                                        @foreach($criteres as $critere => $name)
                                            <option value="{{$critere}}">{{$name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="valeur[]" placeholder="valeur" class="form-control">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="remove"><i class="glyphicon glyphicon-trash"></i> </button>
                                </div>
                            </div>
                        @endif
                    <!-- End Zone de travail -->
                    </div>
                    <div class="col-md-1">
                        <button type="button" id="add"><i class="glyphicon glyphicon-plus"></i> </button>
                    </div>
                </div>
                <hr />
                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Rechercher</button>
            </form>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Liste des clients contenant <span class="label label-default"> {{request('query')}} </span></h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID client</th>
                        <th>Réf. client</th>
                        <th>Nom</th>
                        <th>Prénoms</th>
                        <th>N°compteur</th>
                        <th>Adresse</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @if($clients->count() != 0)
                @foreach($clients as $client)
                    <tr>
                        <td>{{$client->idabon}}</td>
                        <td>{{$client->refbranch}}</td>
                        <td>{{$client->nom}}</td>
                        <td>{{$client->prenoms}}</td>
                        <td>{{$client->numctr}}</td>
                        <td>{{$client->address}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{route('liste_reclamation',['referenceClient' => $client->idabon])}}" class="btn btn-block btn-danger btn-xs" title="Consulter les anciennes réclamations">
                                    <i class="fa fa-folder-open-o"></i> Réclamations
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td colspan="7"><h3 class="text-center">Aucun client trouvé avec vos critères !</h3></td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{$clients->appends(request()->except('page'))->links()}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.content -->
@endsection

@section("script")
<script type="application/javascript">
    $(document).ready(function () {
        $("#add").click(function (e) {
            $("#enveloppe").append($("#template").html());
        });
    });

    $(document).on("click",".remove",function (e) {
        var parent = $(e.target).parent().parent().parent();
        $(parent).fadeOut(function () {
            $(parent).remove();
        });
    });
</script>
@endsection