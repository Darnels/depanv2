@extends('layouts._layout')
@section('content')
<div class="col-md-12">
    <div class="box box-primary col-md-12">
        <h3 class="h3">Liste des équipes <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#myModal">AJouter une équipe</a> </h3>
        <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Login</th>
                <th>Nom de l'équipe</th>
                <th>Date de création</th>
                <th>Tablettes</th>
                <th>Total de réclamations</th>
                <th>Dépannage en cours</th>
                <th>Actions</th>
            </tr>
            <tbody>

            @foreach($equipes as $equipe)
            <tr @if($equipe->appareils->first()) style="background: #b5ffc1;" @endif>
                <td>{{$equipe->login}}</td>
                <td>{{$equipe->libelle}}</td>
                <td>{{(new \Carbon\Carbon($equipe->datecreation))->format('d/m/Y à H:i:s')}}</td>
                <td>{{$equipe->appareils->first() ? $equipe->appareils->first()->libelle : 'Non connecté'}}</td>
                <td>{{$equipe->reclamations->count()}}</td>
                <td>{{$equipe->reclamations->where('status','<>',App\Status::RECLAMATION_TRAITEE_TERMINE)->count()}}</td>
                <td>
                    <div class="btn-group btn-in">
                        <a href="{{route('equipe_profil',["equipe" => $equipe->login])}}" class="btn btn-primary"><i class="fa fa-eye"></i> Voir plus ...</a>
                        <a title="Supprimer l'équipe" href="{{route('equipe_delete',["token" => base64_encode($equipe->login)])}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        <button class="btn btn-primary" data-content="{{$equipe->login}},{{$equipe->libelle}}" data-toggle="modal" data-target="#updatePassword"><i class="fa fa-lock"></i> Mot de passe </button>
                    </div>
                 </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 130px;">
        <form class="form-horizontal" action="{{route('equipe_change_pwd')}}" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modification de mot de passe</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-4">Nom *</label>
                    <div class="col-md-8">
                        <input type="text" name="libelle" id="libelle" placeholder="Nom de l'équipe" class="form-control" disabled required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Mot de passe</label>
                    <div class="col-md-8">
                        <input type="password" name="password" placeholder="Mot de passe" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Confirmation</label>
                    <div class="col-md-8">
                        <input type="password" name="password_confirmation" placeholder="Confirmation" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{csrf_field()}}
                <input type="hidden" name="login" id="login">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 130px;">
        <form class="form-horizontal" action="" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nouvelle équipe</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="control-label col-md-4">Nom *</label>
                    <div class="col-md-8">
                        <input type="text" name="libelle" placeholder="Nom de l'équipe" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Login *</label>
                    <div class="col-md-8">
                        <input type="text" name="login" placeholder="Login de connexion" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Mot de passe</label>
                    <div class="col-md-8">
                        <input type="password" name="password" placeholder="Mot de passe" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Confirmation</label>
                    <div class="col-md-8">
                        <input type="password" name="password_confirmation" placeholder="Confirmation" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{csrf_field()}}
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $('#updatePassword').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('content') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

            var data = recipient.split(",");

            var modal = $(this)
            modal.find('.modal-title').text('Changement mot de passe ' + data[1])
            modal.find('#login').val(data[0]);
            modal.find('#libelle').val(data[1]);
        })
    </script>
@endsection