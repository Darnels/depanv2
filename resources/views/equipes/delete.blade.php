@extends('layouts._layout')
@php(\Carbon\Carbon::setLocale('fr'))
@section('content')
<div class="col-md-8 col-md-offset-2 col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h3>Suppression d'équipe</h3>
        </div>
        <div class="box-body">
            <h4>Voulez-vous vraiment supprimer l'équipe <strong>{{$equipe->libelle}}</strong> ? </h4>
            <p>Cette équipe a été créée le {{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$equipe->datecreation)->format('d/m/Y à H:i:s')}}</p>
            <div>
                <ul>
                    @foreach($equipe->appareils as $appareil)
                        <li>
                            Equipement : {{$appareil->libelle}} -
                            Connecté {{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$appareil->dateconnexion)->diffForHumans()}}
                        </li>

                    @endforeach
                </ul>
            </div>
        </div>
        <div class="box-footer">
            <form method="post" action="" class="form-inline">
                {{csrf_field()}}
                <input type="hidden" value="{{$equipe->login}}" name="login">
                <a href="{{back()->getTargetUrl()}}" class="btn btn-default">Annuler</a>
                <input type="submit" value="Confirmer la suppression" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>
@endsection