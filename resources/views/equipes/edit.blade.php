@extends('layouts._layout')
@php
    \Carbon\Carbon::setLocale('fr');
@endphp
@section('content')
    @if(request()->has("success"))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Succès !</h4>
            {{request()->session()->get("success")}}
        </div>
    @endif
<div class="col-md-3">
    <div class="box box-primary">
        <form class="form-horizontal" method="post" action="" enctype="application/x-www-form-urlencoded">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="{{request()->getBaseUrl()}}/179.jpg" alt="User profile picture">
            <h3 class="profile-username text-center">{{$equipe->libelle}}</h3>
            <p class="text-muted text-center">{{$equipe->login}}</p>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Total de dépannage</b> <a class="pull-right">{{$equipe->reclamations->count()}}</a>
                </li>
                <li class="list-group-item">
                    <b>Dépannages en cours</b> <a class="pull-right">{{$equipe->reclamations->where('status','<>',App\Status::RECLAMATION_TRAITEE_TERMINE)->count()}}</a>
                </li>
                <li class="list-group-item">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$equipe->id}}" />
                    <input type="text" name="libelle" class="form-control" value="{{$equipe->libelle}}"/>
                </li>
            </ul>

            <button type="submit" class="btn btn-primary btn-block"><b>Modifier</b></button>
        </div>
        </form>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Dernière position</h3>
        </div>
        <div class="box-body">
            @if(isset($latestPosition1))
                <iframe src="https://www.google.com/maps/embed/v1/place?key={{\App\Http\Controllers\MapController::MAP_EMBED_KEY}}&q={{$latestPosition->latitude}},{{$latestPosition->longitude}}" frameborder="0" style="border:0" allowfullscreen class="col-md-12 col-sm-12 col-xs-12" height="300"></iframe>
            @else
                <h3>Aucune position trouvée</h3>
            @endif
        </div>
    </div>
</div>
<div class="col-md-9 col-sx-9 col-col-xs-12">
    @if($latestDepannage)
    <section class="box box-primary">
        <div class="box-header with-border">
            <h4>Dernier dépannage</h4>
        </div>
        <div class="box-body">
            <div class="tab-pane" id="timeline">
                <ul class="timeline timeline-inverse">
                    <li class="time-label">
                        <span class="bg-red">
                          Création le {{(new \Carbon\Carbon($latestDepannage->datereclamation))->format('d/m/Y à H:i:s')}}
                        </span>
                    </li>

                    <li>
                        <i class="fa fa-envelope bg-blue"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{(new \Carbon\Carbon($latestDepannage->datereclamation))->diffForHumans()}}</span>

                            <h3 class="timeline-header"><a href="javascript:void(0);">Dépannage </a>N° {{$latestDepannage->numero}}</h3>

                            <div class="timeline-body">
                                Dépannage du client {{$latestDepannage->client->clientName}} {{$latestDepannage->client->clientPrenom}} avec priorité
                                <b>{{$latestDepannage->priorite->libelle}}</b>. <hr/>
                                <p>{{$latestDepannage->localisation}}</p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <i class="fa fa-user bg-aqua"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{(new \Carbon\Carbon($latestDepannage->dateaffectation))->diffForHumans(new Carbon\Carbon($latestDepannage->datereclamation))}}</span>

                            <h3 class="timeline-header no-border"><a href="#">Transfert aux équipes</a> {{$latestDepannage->equipe->libelle}}</h3>
                        </div>
                    </li>

                    @if($latestDepannage->datedemarragenavigation)
                    <li>
                        <i class="fa fa-comments bg-yellow"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{(new \Carbon\Carbon($latestDepannage->datedemarragenavigation))->diffForHumans(new Carbon\Carbon($latestDepannage->dateaffectation))}}</span>

                            <h3 class="timeline-header no-border"><a href="#">Début de la navigation </a> {{$latestDepannage->equipe->libelle}}</h3>
                        </div>
                    </li>
                    @endif
                    @if($latestDepannage->datedebutintervention)
                    <li>
                        <i class="fa fa-comments bg-green"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{(new \Carbon\Carbon($latestDepannage->datedebutintervention))->diffForHumans(new Carbon\Carbon($latestDepannage->datedemarragenavigation))}}</span>

                            <h3 class="timeline-header no-border"><a href="#">Début du dépannage </a> {{$latestDepannage->numero}}</h3>
                        </div>
                    </li>
                    @endif
                    @if($latestDepannage->datefinintervention)
                    <li class="time-label">
                        <span class="bg-fuchsia-active">
                          Fin de dépannage le {{(new \Carbon\Carbon($latestDepannage->datefinintervention))->format('d/m/Y à H:i:s')}}
                        </span>
                    </li>

                    <li>
                        <i class="fa fa-camera bg-purple"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i>{{(new \Carbon\Carbon($latestDepannage->datefinintervention))->diffForHumans(new Carbon\Carbon($latestDepannage->datedebutintervention))}}</span>

                            <h3 class="timeline-header"><a href="#">Rapport d'intervention</a> | Dépannage effectué {{(new \Carbon\Carbon($latestDepannage->datefinintervention))->diffForHumans(new Carbon\Carbon($latestDepannage->datereclamation))}} la création. </h3>

                            <div class="timeline-body">
                                <p>{{$latestDepannage->rapport}}</p>
                            </div>
                        </div>
                    </li>
                    @endif
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    @endif
</div>
@endsection