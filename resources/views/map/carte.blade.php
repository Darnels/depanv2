@extends('layouts._layout')

@section('content')
    <style>
        #map {
            height: 900px;
            min-width: 200px;
        }
        #parentMap{position: relative;}
    </style>
    <div id="parentMap">
        <div id="map"></div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var map;
        var markerArray = new Object();
        var URL = '{{route('liste_tache_auth',['device' => "_device_"])}}';

        //Boucle des appareils et récupération de leur dernière position GPS
        var APPAREILS = [
            @foreach($appareils as $appareil)
            @php( $p = $appareil->positions()->latest('dateposition')->first())
            {
                appareil : '{{$appareil->android_id}}',
                maj : '{{$p->dateposition or 0000-00-00}}',
                equipe: '{{$appareil->libelle}}',
                ordre : {{$p->id or 0}},
                position : {
                    lat : {{$p->latitude or 0.0000}},
                    lng : {{$p->longitude or 0.0000}},
                    speed : {{$p->speed or 0.0000}}
                }
            },
            @endforeach
        ];

        function addMarker(_map,LngLat, equipe, androidID, _ordre, _speed) {
            var marker = new google.maps.Marker({
                map: _map,
                title: equipe,
                animation: google.maps.Animation.DROP,
                position: {lat: LngLat.lat, lng: LngLat.lng},
                icon :'{{request()->getBaseUrl()}}/vehicule.jpg'
            });

            marker.addListener('click',function () {
                $.getJSON(URL.replace('_device_',androidID),function (d) {
                    var infoEquipe = new google.maps.InfoWindow({
                        content: '<div><h3>Equipe '+ equipe +' </h3>'+
                        '<p>Nombre de taches affectées : <strong>'+d.length+'</strong></p>'+
                        '<p>Déplacement à : <strong>'+parseFloat(d.length)*(18/5)+' km/h</strong></p>'+
                        '<p>Dépannage en cours : <strong>'+d[0].numero+'</strong></p>'+
                        '</div>',
                    });

                    infoEquipe.open(_map,marker);
                });
            });

            //Ajout de l'appareil dans la liste des Marqueurs
            var _m = {id:androidID, equipe: equipe, ordre:_ordre , marqueur: marker, speed:_speed};
            markerArray[androidID] = _m;
        }
        
        function updateMarker() {
            $.getJSON('{{route('devices_positions')}}',function (data) {
                $.each(data,function (cle, obj) {
                    //console.log("nouveau :"+obj.ordre + " ancien :"+ markerArray[obj.appareil].ordre)
                    if(!typeof markerArray[obj.appareil] === 'undefined')
                    {
                        if(obj.ordre > markerArray[obj.appareil].ordre ){
                            //markerArray[obj.appareil].marqueur.setPosition({lat: 5.340363 ,lng: -4.006282});
                            markerArray[obj.appareil].marqueur.setPosition({lat: parseFloat(obj.position.lat) ,lng: parseFloat(obj.position.lng)});
                        }
                    }
                })
            })
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat:5.330306, lng:-3.9763826}, //Abidjan
                zoom: 13
            });

            for(var i=0; i < APPAREILS.length; i++) {
                addMarker(map,
                        {lat:APPAREILS[i].position.lat,lng:APPAREILS[i].position.lng},
                        APPAREILS[i].equipe,
                        APPAREILS[i].appareil,
                        APPAREILS[i].ordre,
                        APPAREILS[i].position.speed
                );
            }
            setInterval(updateMarker,10000);
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&callback=initMap"
            async defer>
    </script>
@endsection