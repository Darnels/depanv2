@extends('layouts._layout')
@section('content')
    <style>
        #map {
            height: 900px;
            min-width: 300px;
        }
        #parentMap{position: relative;}
        #controlAffect{
            position: absolute;
            z-index: 999;
            left: 10px;
            top: 1px;
            width: 250px;
        }
    </style>
    <div id="parentMap">
        <div id="map"></div>
        <div id="controlAffect">
            <form method="post" action="" class="" >
                {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Attribution de réclamation aux équipes</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label >N° réclamation</label>
                            <input class="form-control"  placeholder="Numéro de réclamation" type="text" value="{{$reclamation->numero}}" disabled>
                            <input type="hidden" value="{{$reclamation->numero}}" name="reclamation_number">
                            <input type="hidden" value="{{$reclamation->id}}" name="reclamation_id">
                        </div>
                        <div class="form-group">
                            <label for="equipe_id">Appareil android</label>
                            <select class="form-control" name="equipe_id" id="equipe_id">
                                @foreach($appareils as $appareil)
                                <option value="{{$appareil->equipe->id}}">{{$appareil->equipe->libelle}} ({{$appareil->libelle}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Envoyer la reclamation</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

        var reclamationPostion = {lat: {{$reclamation->client->latitude}}, lng: {{$reclamation->client->longitude}} };
        var map;
        var markerArray = new Object();
        var URL = '{{route('liste_tache_auth',['device' => "_device_"])}}';
        var MatrixDistanceURL = '{{route('distance_matrix')}}';

        //Boucle des appareils et récupération de leur dernière position GPS
        var APPAREILS = [
            @foreach($appareils as $appareil)
            @php( $p = $appareil->positions()->latest('dateposition')->first())
            {
                appareil : '{{$appareil->android_id}}',
                equipe: '{{$appareil->equipe->libelle}}',
                maj : '{{$p->dateposition or 0000-00-00}}',
                ordre : {{$p->id or 0}},
                position : {
                    lat : {{$p->latitude or 0.0000}},
                    lng : {{$p->longitude or 0.0000}},
                    speed : {{$p->speed or 0.0000}}
                }
            },
            @endforeach
        ];

        function addMarker(_map,LngLat, equipe, androidID, _ordre, _speed) {
            var marker = new google.maps.Marker({
                map: _map,
                title: equipe,
                animation: google.maps.Animation.DROP,
                position: {lat: LngLat.lat, lng: LngLat.lng},
                icon :'{{request()->getBaseUrl()}}/vehicule.jpg'
            });

            marker.addListener('click',function () {

                //Ajout de l'appareil dans la liste des Marqueurs
                var _m = {id:androidID, ordre:_ordre , equipe: equipe, marqueur: marker, speed:_speed};
                markerArray[androidID] = _m;

                var _origins = markerArray[androidID].marqueur.getPosition().lat()+','+markerArray[androidID].marqueur.getPosition().lng();
                var _destinations = reclamationPostion.lat+','+reclamationPostion.lng;

                $.when($.getJSON(URL.replace('_device_', androidID)),$.post(MatrixDistanceURL,{origins: _origins,destinations: _destinations})).done(function (response1,response2) {

                    var a = response1[2].responseJSON;
                    var b = response2[2].responseJSON;

                    var infoEquipe = new google.maps.InfoWindow({
                        content: '<div><h3>' + equipe + ' </h3> <hr/>' +
                        '<p>Nombre de taches affectées : <strong>' + a.length + '</strong></p>' +
                        '<p>Déplacement à : <strong>' + parseFloat(a.length) * (18 / 5) + ' km/h</strong></p>' +
                        '<p>Dépannage en cours : <strong>' + a[0].numero + '</strong></p>' +
                        '<p>Distance de la panne : <strong>'+b.rows[0].elements[0].distance.text+'</strong></p>'+
                        '<p>Durée estimée : <strong>'+b.rows[0].elements[0].duration.text+'</strong></p>'+
                        '</div>',
                    });
                    infoEquipe.open(_map, marker);
                }); //end of $.wait & done
            });
        };

        function updateMarker() {
            $.getJSON('{{route('devices_positions')}}',function (data) {
                $.each(data,function (cle, obj) {
                    //console.log("nouveau :"+obj.ordre + " ancien :"+ markerArray[obj.appareil].ordre)
                    if(!typeof markerArray[obj.appareil] === 'undefined')
                    {
                        if(obj.ordre > markerArray[obj.appareil].ordre ){
                            //markerArray[obj.appareil].marqueur.setPosition({lat: 5.340363 ,lng: -4.006282});
                            markerArray[obj.appareil].marqueur.setPosition({lat: parseFloat(obj.position.lat) ,lng: parseFloat(obj.position.lng)});
                        }
                    }
                })
            })
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: reclamationPostion,
                zoom: 13
            });

            var markerPanne = new google.maps.Marker({
                position: reclamationPostion,
                map: map,
                title: 'Reclamation {{$reclamation->numero}}',
                icon :'{{request()->getBaseUrl()}}/panne.png'
            });

            for(var i=0; i < APPAREILS.length; i++) {
                addMarker(map,
                    {lat:APPAREILS[i].position.lat,lng:APPAREILS[i].position.lng},
                    APPAREILS[i].equipe,
                    APPAREILS[i].appareil,
                    APPAREILS[i].ordre,
                    APPAREILS[i].position.speed
                );
            }

            var infoPanne = new google.maps.InfoWindow({
                content: '<div><h3>Panne {{$reclamation->numero}}</h3><p>Client :  {{$reclamation->client->clientName}} {{$reclamation->client->clientPrenom}} '+
                '. Maintenance du client de basse tension. '+
                '</p></div>',
            });
            markerPanne.addListener('click',function () {
                infoPanne.open(map,markerPanne);
            });

            setInterval(updateMarker,10000);
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\App\Http\Controllers\MapController::MAP_KEY}}&callback=initMap"
            async defer>
    </script>
@endsection