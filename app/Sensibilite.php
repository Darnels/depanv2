<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 20/04/2017
 * Time: 20:55
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Sensibilite extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'sensibilite';

    const TRES_SENSIBLE = 1;
    const SENSIBLE = 2;
    const NORMALE = 3;
}