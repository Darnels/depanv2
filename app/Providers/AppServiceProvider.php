<?php

namespace App\Providers;

use App\Metier\AccessLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $user = Auth::check() ? Auth::user() : 'Inconnu';
        AccessLog::writeData(
            'IP '.request()->ip().' '.$user.' '.
            request()->method().' '.request()->url().' Data :'.
            json_encode(request()->except('password', 'password_confirmation', 'password_old')));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
