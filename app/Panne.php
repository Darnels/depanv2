<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 24/02/2017
 * Time: 15:57
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Panne extends Model
{
    protected $table = 'panne';
    protected $guarded = [];
    public $timestamps = false;
}