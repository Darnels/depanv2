<?php

namespace App\Http\Controllers;

use App\Appareil;
use App\Client;
use App\Metier\ClientProcessing;
use App\Utilisateur;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchClientController extends Controller
{
    use ClientProcessing;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchClient(Request $request)
    {
        $clientsfound = null;

        if($request->has("critere")){
            $clientsfound = $this->processRequest($request);
        }else{
            $clientsfound = $this->getClientByKeyword($request->input('query'));
        }

        if($clientsfound->count() == 1) {
            //Si un seul client trouvé
            return redirect()->route('liste_reclamation',["referenceClient" => $clientsfound->first()->idabon]);
        }else {//Si plusieurs lignes retournées
            return $this->showViewForMany($clientsfound);
        }
    }

    public function processRequest(Request $request)
    {
        $raw = Client::orderBy("nom");
        foreach ($request->query("critere") as $index => $criteres)
        {
            $raw->where($criteres,'like',sprintf("%s%%",$request->query("valeur")[$index]));
        }
        return $raw->paginate(30);
    }
}