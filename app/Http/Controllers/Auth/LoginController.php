<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Status;
use App\Utilisateur;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 2; //2 +1 tentatives => 3 tentatives au final
    protected $decayMinutes = 5; // 5 min =>

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/accueil';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if((new Carbon($user->dhms_pwd_update))->diffInDays(Carbon::now()) > intval(env("PASSWORD_EXPIRE",90))){
            $user->statut = Status::UTILISATEUR_BLOQUE;
            $user->update();

            return redirect()->route('change_password')
                ->withErrors('Votre mot de passe a expiré. Veuillez en créer un autre SVP !');
        }else{
            return null;
        }
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $intercept = $this->authenticated($request, $this->guard()->user());

        return $intercept ? $intercept : redirect()->intended($this->redirectPath());
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws  \Throwable
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            //Bloquer le compte utlisateur
            $this->blockUser($request->input($this->username()));

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if($this->checkUserStatus($this->guard()->user())){
                //Si le compte est au statut Actif ou provisoire encore valide
                return $this->sendLoginResponse($request);
            }else{
                //On le déconnecte
                $this->guard()->logout();
                $request->session()->invalidate();

                //Sinon le statut est soit bloqué ou Inactif
                return redirect()->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([ $this->username() => "Votre compte est bloqué ! Veuillez contacter votre administrateur svp."]);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @param $email
     * @return bool
     * @throws \Throwable
     */
    protected function blockUser($email)
    {
        try{
            $user = Utilisateur::where("email", $email)->firstOrFail();
            $user->statut = Status::UTILISATEUR_BLOQUE;
            $user->saveOrFail();
        }catch (ModelNotFoundException $e){
            return false;
        }
        return true;
    }

    /**
     * @param Authenticatable $utilisateur
     * @return bool
     */
    protected function checkUserStatus(Authenticatable $utilisateur)
    {
        if(in_array(intval($utilisateur->statut), [Status::UTILISATEUR_BLOQUE, Status::UTILISATEUR_INACTIF], true))
        {
            return false;  //Le statut du compte n'autorise pas de connexion
        }elseif(intval($utilisateur->statut) == Status::UTILISATEUR_PROVISOIRE){
            //Tant que le résultat est négaatif le compte est valide Date du jour - Date d'expiration
            if((new Carbon($utilisateur->expire_at))->diffInDays(Carbon::now(), false) < 0){
                return true; //Le temps de validité est encore disponible
            }
            return false; //Le temps de validité a expiré
        }else{ //Statut = Actif (1)
            return true;
        }
    }
}