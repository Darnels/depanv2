<?php

namespace App\Http\Controllers;

use App\Famille;
use App\Metier\ReclamationProcessing;
use App\Reclamation;
use App\Statistics\Bon;
use App\Statistics\Depannage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CrcController extends Controller
{
    use ReclamationProcessing, Depannage, Bon;

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function showEtatReclamation(Request $request)
    {
        try{
            $familly = $this->statistiquesByFamilly();
            $reclamation = $this->countReclamation();
            $bon = (new Collection($this->statistiqueBonByState()))->first();
            $bons = (new Collection($this->statistiqueBonByAffect()))->first();
            $inputs = new Collection([
                'familles' =>Famille::all()
            ]);
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            return back()->withErrors($e->getMessage())->withInput();
        }

        return view('crc.etatmensuel',compact("reclamation","familly","bon","inputs","bons"));
    }


    private function countReclamation()
    {
        $today = Carbon::now();
        $reclamationMensuel = Reclamation::whereBetween("datereclamation",[
            $today->firstOfMonth()->toDateTimeString(),
            $today->lastOfMonth()->toDateString()." 23:59:59"
        ])->count();

        $reclamationAnnuel = Reclamation::whereYear("datereclamation",$today->year)->count();

        return new Collection(compact("reclamationMensuel","reclamationAnnuel"));
    }

    public function ajaxNatures(Request $request, $code){
        $raw = $this->statistiquesByNature($code);
        $data = [
            'libelles' => array_column($raw->toArray(), 'libelle'), //["January", "February", "March", "April", "May", "June", "July"],
            'total' => array_column($raw->toArray(), 'total') //[65, 59, 80, 81, 56, 55, 40],
        ];

        return response()->json($data,200,[
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        ],
        JSON_UNESCAPED_UNICODE);
    }
}