<?php

namespace App\Http\Controllers;

use App\Metier\ReclamationProcessing;
use App\Reclamation;
use App\Status;
use Carbon\Carbon;
use Illuminate\Cookie\CookieJar;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class HomeController extends Controller
{
    use ReclamationProcessing;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showDepannagesInfos()
    {
        $reclamations = $this->getReclamations('<>',Status::RECLAMATION_TRAITEE_TERMINE);
        return view('home',compact("reclamations"),[
            "X-Frame-Options" =>"ALLOW-FROM https://www.google.com/"
            ]);
    }

    public function index(Request $request)
    {
        $getParameters = $request->query();

        //Dates & période
        $collection = new  Collection();

        $collection->put('dateDebut',($request->has('periodedebut') ?
            Carbon::createFromFormat('d/m/Y',$request->query('periodedebut')) :
            Carbon::now()->firstOfMonth()));

        $collection->put('dateFin',($request->has('periodefin') ?
            Carbon::createFromFormat('d/m/Y',$request->query('periodefin')) :
            Carbon::now()));

        $reclamations = Reclamation::with('client','priorite','equipe')
            ->whereBetween("datereclamation",[$collection->get('dateDebut')
            ->toDateString(),$collection->get('dateFin')
            ->toDateString().' 23:59:59'])->get();

        $timeToLive = 60*24*30; //minutes * heures * nombre jours = 1 mois de 30 jours

        return response(view('dashboard',compact("reclamations","collection")))
            ->withCookie(cookie('email',Auth::user()->email,$timeToLive));
    }
}