<?php

namespace App\Http\Controllers;

use App\Appareil;
use App\Reclamation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AppareilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showListeEquipe()
    {
        $appareils = Appareil::all();
        return view('appareils.liste',compact("appareils"));
    }

    public function showAppareilProfil($androidID)
    {
        $appareil = Appareil::with('equipe')->where('android_id',$androidID)->first();
        $latestPosition = $appareil->positions()->latest('dateposition')->first();
        $latestDepannage = null;
        //$latestDepannage = Reclamation::where('appareil_id',$appareil->id)->orderBy('dateaffectation','desc')->first();
        return view('appareils.edit',compact("appareil","latestPosition","latestDepannage"));
    }

    public function updateAppareil(Request $request){
        $this->validRequest($request);
        try{
            $appareil = Appareil::findOrFail($request->input('id'));
            $appareil->libelle = $request->input('libelle');
            $appareil->saveOrFail();

            $request->session()->flash("success","Nom de l'appareil modifié avec succès");
            return back();
        }catch (ModelNotFoundException $e){
            Log::error($e->getTraceAsString());
            return back()->withErrors('Mise à jour impossible, veuillez recommencer plus tard.');
        }
    }

    public function validRequest(Request $request){
        $this->validate($request,[
            'libelle' => "required",
            'id' => 'required|numeric'
        ],[
            'libelle.required' => "Le nom de l'appareil est requis",
            'id.required' => "Identifiant introuvable."
        ]);
    }
}
