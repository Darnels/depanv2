<?php

namespace App\Http\Controllers;

use App\Appareil;
use Illuminate\Http\Request;

class MapController extends Controller
{
    const MAP_KEY = "AIzaSyA7nl4IYZVJCWievWF7yv8xu8-WFind8CM";
    const MAP_EMBED_KEY = "AIzaSyAMwTZk-cPjxl57xnmeCxldnE8_1qczEas";

    const CIE_DEFAULT_LATITUDE = "5.297368";
    const CIE_DEFAULT_LONGITUDE = "-3.990154";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showMapDevice()
    {
        $appareils = Appareil::all();
        return view('map.carte',compact("appareils"));
    }
}
