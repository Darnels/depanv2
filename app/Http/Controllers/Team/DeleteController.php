<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 22/06/2017
 * Time: 13:38
 */

namespace App\Http\Controllers\Team;


use App\Equipe;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function showDeleteForm($token){
        try{
            $equipe = $this->getEquipe($token);
        }catch (ModelNotFoundException $e){
            return back()->withErrors("Cette équipe n'existe pas.");
        }
        return view('equipes.delete',compact("equipe"));
    }

    public function delete(Request $request, $token){
        $this->validate($request,[
            "login" => "required|exists:equipe"
        ]);

        try{
            $equipe = $this->getEquipe($token);
            $equipe->delete();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors("Cette équipe n'existe pas.");
        }
        return redirect()->route('equipe_liste')->with("success","Equipe supprimée avec succèss");
    }

    private function getEquipe($token){
        return Equipe::where("login",base64_decode($token))
            ->with('appareils')
            ->firstOrFail();
    }
}