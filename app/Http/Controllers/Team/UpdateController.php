<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 18/06/2017
 * Time: 19:39
 */

namespace App\Http\Controllers\Team;


use App\Equipe;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function updateEquipe(Request $request){
        $this->validRequest($request);
        try{
            $equipe = Equipe::findOrFail($request->input('id'));
            $equipe->libelle = $request->input('libelle');
            $equipe->saveOrFail();

            $request->session()->flash("success","Nom de l'équipe modifié avec succès");
            return back();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors('Mise à jour impossible, veuillez recommencer plus tard.');
        }
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request,[
            "login" => "required|exists:equipe",
            "password" => "required|min:6|confirmed"
        ]);

        try{
            $equipe = Equipe::where("login",$request->input("login"))->first();
            $equipe->password = bcrypt($request->input("password"));

            $equipe->saveOrFail();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors('Mise à jour impossible, veuillez recommencer plus tard.');
        }

        return  back()->with("success","Mot de passe de l'équipe modifié avec succès.");
    }

    public function validRequest(Request $request){
        $this->validate($request,[
            'libelle' => "required",
            'id' => 'required|numeric'
        ],[
            'libelle.required' => "Le nom de l'équipe est requis",
            'id.required' => "Identifiant introuvable."
        ]);
    }
}