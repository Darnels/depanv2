<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 18/06/2017
 * Time: 19:19
 */

namespace App\Http\Controllers\Team;


use App\Equipe;
use App\Http\Controllers\Controller;
use App\Reclamation;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showEquipeProfil($equipeLogin)
    {
        $equipe = Equipe::where('login',$equipeLogin)->first();
        $latestPosition = $equipe->appareils(); //->positions()->latest('dateposition')->first();
        $latestDepannage = Reclamation::where('equipe_id',$equipe->id)->orderBy('dateaffectation','desc')->first();

        return view('equipes.edit',compact("equipe","latestPosition","latestDepannage"));
    }
}