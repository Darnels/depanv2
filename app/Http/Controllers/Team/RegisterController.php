<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 17/06/2017
 * Time: 12:39
 */

namespace App\Http\Controllers\Team;


use App\Equipe;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showListe()
    {
        $equipes = Equipe::with("appareils")->get();
        return view('equipes.liste',compact("equipes"));
    }

    public function register(Request $request){
        //validate request
        $this->validate($request,$this->validateRules());

        try{
            $this->create($request->except("_token","password_confirmation"));
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors("Erreur ! Une erreur empêche la création de l'équipe.");
        }
        return back()->with("success","Nouvelle équipe crée avec succès");
    }

    private function create(array $data){
        $collection = new Collection($data);
        $collection->put("datecreation",Carbon::now()->toDateTimeString());
        $collection->put("password",bcrypt($data["password"]));

        Equipe::create($collection->toArray());
    }

    private function validateRules(){
        return [
            "login" => "required|unique:equipe,login",
            "password" => "required|min:6|confirmed",
            "libelle" => "required",
        ];
    }
}