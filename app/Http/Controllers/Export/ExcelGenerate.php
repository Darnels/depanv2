<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 02/10/2017
 * Time: 12:23
 */

namespace App\Http\Controllers\Export;
use Maatwebsite\Excel\Facades\Excel;

trait ExcelGenerate
{
    public function exportListeReclamation(array $data=null)
    {
        $data = $this->convertToArray($data);

        return Excel::create('Liste_reclamations', function($excel) use ($data) {

            // Set the title
            $excel->setTitle('Liste des réclamations');

            // Chain the setters
            $excel->setCreator('DepanV2')
                ->setCompany('Djera Services');

            // Call them separately
            $excel->setDescription('Liste des réclamation exportée de l\'application DepanV2 ');

            $excel->sheet("RECLAMATIONS", function ($sheet) use ($data){
                $sheet->fromArray($data);
            });

        })->download("xlsx");
    }

    /**
     * @param $data
     * @return array
     */
    private function convertToArray($data)
    {
        $array = [];
        foreach ($data as $value)
        {
            $raw = null;

            if(is_object($value))
            {
                $raw = get_object_vars($value);
            }

            if(is_array($raw)){
                $array[] = $raw;
            }
        }

        return $array;
    }
}