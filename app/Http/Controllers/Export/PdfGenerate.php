<?php

namespace App\Http\Controllers\Export;

use Barryvdh\DomPDF\Facade as PDF;

trait PdfGenerate
{
    public function printListeReclamations($data=null)
    {
        $liste = PDF::loadView('pdf.liste_type_bon', compact("data"))->setPaper('a4','landscape');
        return $liste->download('Liste_reclamations.pdf');
    }
}