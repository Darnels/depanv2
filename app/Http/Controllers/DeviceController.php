<?php

namespace App\Http\Controllers;

use App\Appareil;
use App\CompteRendu;
use App\Famille;
use App\Metier\AppareilProcessing;
use App\Metier\ReclamationProcessing;
use App\Nature;
use App\PositionAppareil;
use App\Reclamation;
use App\Equipe;
use App\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class DeviceController extends Controller
{
    use AppareilProcessing, ReclamationProcessing;

    public function __construct()
    {
        $this->middleware('guest')->except(['getTachesByDevicesAuth','getLatestPositionFromAll','getGoogleDistanceMatrixInfos']);
    }

    public function registerToken(Request $request)
    {
        $message = null;
        $this->validateDeviceToken($request);

        $device = $this->getDeviceByAndroidID($request->input('android_id'));

        if(!$device->exists) //S'il n'existe pas
        {
            $device->token = $request->input('token');
            $message = "L'appareil a été enregistré avec succès";
            $device->libelle = 'Nouvel équipement '.Carbon::now()->toDateTimeString();
            $device->dateconnexion = Carbon::now()->toDateTimeString();
        }else{
            $device->token = $request->input('token');
            $message = "L'appareil a été déjà enregistré";
        }

        try{
            $device->saveOrFail();
        }catch (ModelNotFoundException $e)  {
            Log::error("device existe ::: ".$device->toJson());
            Log::error($e->getTraceAsString());
        }


        return response()->json([
            "status" => "OK",
            "message" => $message,
            "appareil" => $device
        ]);
    }

    public function registerLocation(Request $request)
    {
        $d = $request->input();
        $d['date'] = Carbon::now()->toDateTimeString();
        //Log::info(json_encode($d));
        $positionAppareil = new PositionAppareil($request->except('android_id'));
        try{
            $positionAppareil->dateposition = Carbon::now()->toDateTimeString();
            $positionAppareil->appareil_id = Appareil::where('android_id',$request->input('android_id'))->firstOrFail()->id;
            $positionAppareil->save();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    public function getTacheByDevice($equipe,$lastID = 0)
    {
        try{
            $reclamations = Reclamation::with('client','priorite')
                ->where('equipe_id',$equipe)
                ->where('status',Status::RECLAMATION_TRANSFEREE_EQUIPE)
                ->where('id','>',$lastID)
                ->orderBy('priorite_id','asc')
                ->orderBy('sensibilite_id','asc')
                ->orderBy('dateaffectation','asc')
                ->get();

            $retour = [];

            foreach ($reclamations as $reclamation){
                $retour[] = [
                    "client" => $reclamation->client->nom." ".$reclamation->client->prenoms,
                    "Panne" => $this->getMessageForPush($reclamation),
                    "dateaffectation" => (new Carbon($reclamation->dateaffectation))->toDateTimeString(),
                    "long" => $reclamation->client->longitude,
                    "lat" => $reclamation->client->latitude,
                    "numero" => $reclamation->numero,
                    "id" => $reclamation->id,
                    "priorite" => $reclamation->priorite->id
                ];
            }

            return response()->json([
                "equipe" => "Equipe",
                "data" => $retour
                ], 200, [
                "Content-type" => "application/json; charset=utf-8"
            ],JSON_UNESCAPED_UNICODE);

        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }catch (\Exception $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        return response('',500);
    }

    public function getTachesByDevicesAuth($device){
        return $this->getTacheByDevice($device);
    }

    public function getLatestPositionFromAll(){
        $appareils = Appareil::all();
        $json = [];
        foreach($appareils as $appareil) {
            $p = $appareil->positions()->latest('dateposition')->first();
            $json[]=[
                "appareil" => $appareil->android_id,
                "maj" => $p ? $p->dateposition : Carbon::now()->toDateTimeString(),
                "equipe" => $appareil->libelle,
                "ordre" => $p ? $p->id : 0,
                "position" => [
                    "lat" => $p ? $p->latitude : MapController::CIE_DEFAULT_LATITUDE,
                    "lng" => $p ? $p->longitude : MapController::CIE_DEFAULT_LONGITUDE ,
                    "speed" => $p ? $p->speed : '00.00'
                ]
            ];
        }

        return response()->json($json,200,[
            "Content-type" => "application/json; charset=utf-8"
        ]);
    }

    public function addReport(Request $request)
    {
        $msg = null;
        $code = 0;
        $reclamation = $this->getReclamationByNumber($request->input('numero'));

        try{
            //$reclamation->rapport = $request->input('rapport');
            $reclamation->status = Status::RECLAMATION_TRAITEE_TERMINE;
            $reclamation->datefinintervention = Carbon::now()->toDateTimeString();
            $reclamation->saveOrFail();

            //Save in compte rendu table
            $cr = new CompteRendu();
            $cr->dateheurecontact = Carbon::createFromFormat('d/m/Y H:i:s',$request->input('dateheurecontact').":00")->toDateTimeString();
            $cr->dateheuredebutintervention = Carbon::createFromFormat('d/m/Y H:i:s',$request->input('dateheuredebutintervention').":00")->toDateTimeString();
            $cr->dateheurefinintervention = Carbon::now()->toDateTimeString();
            $cr->detailrapport = $request->input('detailrapport');
            $cr->statut = $request->input('statut');
            $cr->assistancexterne = $request->input('assistancexterne');

            if($request->input('assistancexterne') != "Aucun"){
                $cr->dateheuredemande = Carbon::createFromFormat('d/m/Y H:i',$request->input('dateheuredemande'))->toDateTimeString();
                $cr->dateheurearrivee = Carbon::createFromFormat('d/m/Y H:i',$request->input('dateheurearrivee'))->toDateTimeString();
            }

            $cr->equipe_id = ( $request->has("equipe_id") ? $request->input("equipe_id") : $reclamation->equipe_id );

            $cr->nature_id = $request->input('nature_id');
            $cr->reclamation_id = $reclamation->id;
            $cr->saveOrFail();

            $msg = "Rapport de la réclamation ".$reclamation->numero." bien enregistré";
        }catch (ModelNotFoundException $e){
            $msg = "Une erreur a empêché l'enregistrement du rapport";
            $code = 1;
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }catch (\Exception $e ){
            $msg = "Une erreur a empêché l'enregistrement du rapport";
            $code = 1;
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return response()->json([
            "message" => $msg,
            "code" => $code
        ],200,[
            "Content-Type" => "application/json; charset=utf-8"
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getGoogleDistanceMatrixInfos(Request $request){
        return response($this->getDataFromGoogleMatrixAPI($request->input('origins'),$request->input('destinations')),200,[
            "Content-type" => "application/json; charset=utf-8"
        ]);
    }

    public function updateReclamationStatus(Request $request){
        $this->validate($request, [
            "android_id" => "required",
            "numero" => "required"
        ]);

        $update = new Collection($request->except('android_id','numero'));

        if($update->has("datedemarragenavigation"))
            $update->put("datedemarragenavigation",Carbon::now()->toDateTimeString());

        if($update->has("dateheurefinnavigation"))
            $update->put("dateheurefinnavigation",Carbon::now()->toDateTimeString());

        if($update->has("datedebutintervention"))
            $update->put("datedebutintervention",Carbon::now()->toDateTimeString());

        try {
            $reclamation = Reclamation::where('numero', $request->input('numero'))->firstOrFail();
            $reclamation->update($update->toArray());
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return response()->json([
          "datedebutintervention" => $update->get("datedebutintervention"),
          "dateheurefinnavigation" => $update->get("dateheurefinnavigation"),
          "datedemarragenavigation" => $update->get("datedemarragenavigation"),
        ],200,[
            "Content-Type" => "application/json; charset=utf-8"
        ], JSON_UNESCAPED_UNICODE);
    }

    //@protected
    private function getDataFromGoogleMatrixAPI($origins, $destinations){
        $GoogleDistanceMatrixURL = "https://maps.googleapis.com/maps/api/distancematrix/json?key=".MapController::MAP_KEY."&origins=".$origins."&destinations=".$destinations;

        $target = curl_init($GoogleDistanceMatrixURL);
        curl_setopt($target,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($target,CURLOPT_RETURNTRANSFER,true);

        $resultat = curl_exec($target);

        if($resultat)
            return $resultat;
        else
            throwException(new \Exception('Google Distance Matrix API error'));
    }

    public function getFamille()
    {
        $famille = new Famille();
        $famille->setVisible(['code','libelle']);
        return response()->json($famille::all(),200,[
                "Content-Type" => "application/json; charset=utf-8"
            ], JSON_UNESCAPED_UNICODE);
    }

    public function getNatureByFamilly(){
        $code = request('code');

        $nature = new Nature();
        $nature->setVisible(['*']);

        return response()->json($nature::where('famille_code',$code)->get(),200,[
            "Content-Type" => "application/json; charset=utf-8"
            ], JSON_UNESCAPED_UNICODE);
    }

    public function getTeam()
    {
        return response()->json(Equipe::all(),200,[
            "Content-Type" => "application/json; charset=utf-8"
        ], JSON_UNESCAPED_UNICODE);
    }

    public function authEquipe(Request $request){
        $code = 0;

        $equipe = Equipe::with("appareils")->where('login',$request->input('login'))
            ->first();

        if($equipe){ //test de récupération de l'équipe

            $message = "Mot de passe incorrect";

            if($equipe->appareils->count() != 0){
                $message = "L'équipe est déjà connectée sur une autre tablette";

            }else{

                if(Hash::check($request->input('password'),$equipe->password)){

                    $code = 1;
                    $message = "Authentification de l'équipe réussie !";

                    //On met à jour les infos de l'appareil
                    $device = $this->getDeviceByAndroidID($request->input('android_id'));
                    $device->equipe()->associate($equipe);
                    $device->dateconnexion = Carbon::now()->toDateTimeString();
                    $device->save();

                }
            }
        }else{
            $message = "Equipe introuvable";
        }

        return response()->json(compact("code","message"),200,[
            "Content-Type" => "application/json; charset=utf-8"
        ], JSON_UNESCAPED_UNICODE);
    }

    public function logoutEquipe(Request $request){
        Log::error(json_encode($request->all()));
        //On met à jour les infos de l'appareil
        $device = $this->getDeviceByAndroidID($request->input('android_id'));
        $device->equipe_id = null;
        $device->dateconnexion = Carbon::now()->toDateTimeString();
        $device->save();
    }
}