<?php

namespace App\Http\Controllers;

use App\CanalCommunication;
use App\Client;
use App\ClientInfos;
use App\CompteRendu;
use App\Exploitation;
use App\Firebase\Push;
use App\GpsClient;
use App\Metier\AppareilProcessing;
use App\Metier\ReclamationProcessing;
use App\Panne;
use App\Priorite;
use App\Reclamation;
use App\Sensibilite;
use App\Statistics\Bon;
use App\Status;
use App\TypeReclamation;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReclamationController extends Controller
{
    const PREFIX_RECLAMATION = 'BT';
    const DEFAULT_NUMBER_RECLAMATION = "000000000000000";

    use ReclamationProcessing, AppareilProcessing, Bon;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showNewReclamation($referenceClient)
    {
        try{
            $client = Client::where('idabon',$referenceClient)
                ->firstOrFail();

            $pannes = Panne::all();
            $exploitations = Exploitation::with('direction')->get();
            $canalCommunication = CanalCommunication::all();
            $oldReclamations = Reclamation::where("client_id",$client->id)->orderBy('datereclamation','desc')->get();
            $typeReclamation = TypeReclamation::all();
            $priorites = Priorite::all();
            $sensibilites = Sensibilite::all();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors([])->withInput();
        }

        return view('reclamation.nouvelle',compact("client","pannes","exploitations","oldReclamations","canalCommunication","typeReclamation","priorites","sensibilites"));
    }

    public function showListeReclamation($referenceClient)
    {
        try{
            $client = Client::where('idabon',$referenceClient)
                ->firstOrFail();

            $pannes = Panne::all();
            $exploitations = Exploitation::with('direction')->get();
            $canalCommunication = CanalCommunication::all();
            $oldReclamations = Reclamation::where("client_id",$client->id)->orderBy('datereclamation','desc')->get();
            $typeReclamation = TypeReclamation::all();
            $priorites = Priorite::all();
            $sensibilites = Sensibilite::all();

        }catch (ModelNotFoundException $e){

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors([])->withInput();
        }

        return view('reclamation.nouvellerecap',compact("client","pannes","exploitations","oldReclamations","canalCommunication","typeReclamation","priorites","sensibilites"));
    }

    public function saveReclamation(Request $request)
    {
        $this->validateReclamation($request);
        $reclamation = new Reclamation($request->except("_token","longitude","latitude","updategps"));
        $reclamation->datereclamation = Carbon::now()->toDateTimeString();

        try{
            //MAJ des coordonnées du client si le client à signalé une autre panne
            if($request->input('updategps') != 0){
                $client = Client::findOrFail($request->input('client_id'));
                $client->longitude = $request->input('longitude');
                $client->latitude = $request->input('latitude');
                $client->saveOrFail();
            }

            if($request->has('depanageenligne')){
                $reclamation->status = Status::RECLAMATION_TRAITEE_TERMINE;
                $reclamation->datefinintervention = Carbon::now()->toDateTimeString();
            }else{
                $reclamation->status = Status::RECLAMATION_CREEE;
            }

            $reclamation->numero = self::DEFAULT_NUMBER_RECLAMATION;
            $reclamation->utilisateur_id = Auth::user()->id;
            $reclamation->saveOrFail();

            $reclamation->numero = $this->generateNumber($reclamation);
            $reclamation->saveOrFail();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withInput()->withErrors("La reclamation n'a pas pu être enregistrée.<small>".$e->getMessage()."</small>");
        }

        return redirect()->route("accueil")->with(\App\Notify\Notification::NOTIFICATION_KEY,"La réclamation à bien été enregistrée");
        //return redirect()->route("plannification_bt",['reclamationNumber' => substr($reclamation->numero,strpos($reclamation->numero,self::PREFIX_RECLAMATION,0))]);
    }

    public function showUpdateReclamation($reclamationNumber)
    {
        try{
            $reclamation = $this->getReclamationEndNumero($reclamationNumber);

            $pannes = Panne::all();
            $exploitations = Exploitation::with('direction')->get();
            $canalCommunication = CanalCommunication::all();
            $oldReclamations = Reclamation::where("client_id",$reclamation->client->id)->orderBy('datereclamation','desc')->get();
            $typeReclamation = TypeReclamation::all();
            $priorites = Priorite::all();
            $sensibilites = Sensibilite::all();
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors([])->withInput();
        }


        return view('reclamation.modifier',compact("client","pannes","exploitations","oldReclamations","canalCommunication","typeReclamation","priorites","reclamation","sensibilites"));
    }

    public function updateReclamation(Request $request){
        //dd($request);
        $this->validateReclamation($request);
        try{
            $reclamation = $this->getReclamationByNumber($request->input('numero'));
            $reclamation->__construct($request->except("_token","latitude","longitude","updategps"));

            if($request->has('relance')){
                $reclamation->relance = true;
                $reclamation->dateheurerelance = Carbon::now()->toDateTimeString();

                //Send Notification to Android Device
                $push = new Push("Relance intervention",$this->getMessageForPush($reclamation));
                $this->sendNotification($push,$reclamation->equipe->appareils);
            }

            if($request->has('depanageenligne'))
                $reclamation->status = Status::RECLAMATION_TRAITEE_TERMINE;

            $reclamation->saveOrFail();
        }Catch(ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withInput()->withErrors("Une erreur s'est produite lors de la mise à jour de la réclamtion");
        }

        return redirect()->route("accueil")->with(\App\Notify\Notification::NOTIFICATION_KEY, "La notification a été modifiée");
    }

    public function detailUpReclamation(Request $request){
        $this->validate($request,[
            "rapportcrc" => 'required'
        ]);

        try{
            $reclamation = $this->getReclamationByNumber($request->input('numero'));
            $reclamation->__construct($request->except("_token"));
            $reclamation->saveOrFail();

        }Catch(ModelNotFoundException $e){

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withInput()->withErrors('Une erreur s\'est produite lors de la mise à jour de la réclamtion');
        }

        return redirect()->route("accueil");
    }

    public function showListEncours()
    {
        $encours = $this->getReclamations('=',Status::RECLAMATION_TRANSFEREE_EQUIPE);
        return $this->showList($encours," en cours");
    }

    public function showListClose()
    {
        $close = $this->getReclamations('=',Status::RECLAMATION_TRAITEE_TERMINE);
        return $this->showList($close, " terminées");
    }

    public function showListWait()
    {
        $wait = $this->getReclamations('=',Status::RECLAMATION_CREEE);
        return $this->showList($wait, " en attentes d'affectation");
    }

    public function showListDate(Request $request)
    {
        $datedebut = new Carbon();
        $datefin = new Carbon();
        if ($request->has('periodedebut') && $request->has('periodefin'))
        {
            $datedebut = Carbon::createFromFormat("d/m/Y", $request->input('periodedebut'));
            $datefin = Carbon::createFromFormat("d/m/Y", $request->input('periodefin'));
        }else {
            $datedebut = $datedebut->firstOfMonth();
        }

        $reclamations = $this->listBonByCall($datedebut, $datefin);
        return view('crc.etatliste', compact("reclamations"));
    }

    public function showListMai(Request $request)
    {
        return $this->sendViewListe(CompteRendu::MAI, $request);
    }

    public function showListPro(Request $request)
    {
        return $this->sendViewListe(CompteRendu::PRO, $request);
    }

    public function showListDef(Request $request)
    {
        return $this->sendViewListe(CompteRendu::DEF, $request);
    }

    public function showDetailReclamation($reclamationNumber)
    {
        try {
            $reclamation = $this->getReclamationEndNumero($reclamationNumber);

            return view('reclamation.details',compact("reclamation"));
        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors("Reclamation introuvable");
        }
    }

    private function validateReclamation(Request $request)
    {
        $this->validate($request,[
            "localisation" => 'required',
            "canalcommunication_id" => 'required|numeric',
            "panne_id" => 'required|numeric',
            "typereclamation_id" => 'required|numeric',
            "exploitation_id" => 'required|numeric',
            "client_id" => 'required|numeric',

            "updategps" => 'required_with:longitude,latitude|numeric',
            "longitude" => 'required_if:updategps,*|numeric',
            "latitude" => 'required_if:updategps,*|numeric',

            "nomappelant" => "required_if:nonabonne,1",
            "contactappelant" => "required_if:nonabonne,1",
        ],[
            "nomappelant.required_if" => "Le nom de l'appelant est requis lorsque l'interlocuteur n'est pas client",
            "contactappelant.required_if" => "Le numéro de l'appelant est requis lorsque l'interlocuteur n'est pas client",
        ]);
    }

    private function generateNumber(Reclamation $reclamation)
    {
        $typeReclamation = TypeReclamation::find($reclamation->typereclamation_id);
        $exploitation = Exploitation::find($reclamation->exploitation_id);

        $date = new Carbon($reclamation->datereclamation);

        return $typeReclamation->code.$exploitation->code.$date->format("m").$reclamation->id.$date->format("Y");
    }

    private function showList(LengthAwarePaginator $data,$type)
    {
        $reclamations = $data;
        return view('reclamation.liste',compact('reclamations',"type"));
    }
}