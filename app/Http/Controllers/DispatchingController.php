<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 25/02/2017
 * Time: 14:23
 */

namespace App\Http\Controllers;


use App\Appareil;
use App\Equipe;
use App\Firebase\FirebaseBase;
use App\Firebase\FirebaseException;
use App\Firebase\Push;
use App\Metier\AppareilProcessing;
use App\Metier\ReclamationProcessing;
use App\Reclamation;
use App\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DispatchingController extends Controller
{
    use ReclamationProcessing, AppareilProcessing;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showMapForReclamation($reclamationNumber)
    {
        $appareils = Appareil::with('equipe')
                    ->whereNotNull('equipe_id')
                    ->get();

        try{
            $reclamation = $this->getReclamationEndNumero($reclamationNumber);
        }catch (ModelNotFoundException $e){
            return back()->withErrors('Reclamation introuvable');
        }
        return view('map.affectation',compact("reclamation","appareils"),[
            "X-Frame-Options" =>"ALLOW-FROM https://maps.googleapis.com/"
        ]);
    }

    public function assignReclamationToDevice(Request  $request)
    {
        try{
            $reclamation = $this->getReclamationEndNumero($request->input('reclamation_number'));

            if(!$reclamation->exists())
                throw new ModelNotFoundException();

            // verification d'une réaffectation
            if($request->has("reaffect") && $request->input("reaffect")== "true")
            {
                $this->changeEquipe($reclamation);
            }


            //Send Notification to Android Device
            $push = new Push("Nouvelle intervention",$this->getMessageForPush($reclamation));
            $equipe = Equipe::with('appareils')->find($request->input('equipe_id'));

            $this->sendNotification($push,$equipe->appareils);

            $reclamation->status = Status::RECLAMATION_TRANSFEREE_EQUIPE;
            $reclamation->equipe_id = $request->input('equipe_id');
            $reclamation->dateaffectation = Carbon::now()->toDateTimeString();
            $reclamation->saveOrFail();

        }catch (ModelNotFoundException $e){
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return back()->withErrors('Reclamation introuvable');
        }catch (FirebaseException $e){
            return back()->withErrors($e->explainError($e->getCode()));
        }

        return redirect()->route('accueil')->with(\App\Notify\Notification::NOTIFICATION_KEY,"La réclamation a bien été affectée");
    }

    private function changeEquipe(Reclamation &$reclamation)
    {
        //MAJ des infos de reclamation
        $reclamation->oldequipe_id = $reclamation->equipe_id;
        $reclamation->datereaffectation = Carbon::now()->toDateTimeString();

        //Send Notification to Android Device
        $push = new Push("Bon retiré",$this->getMessageForLostReclamation($reclamation));
        $equipe = Equipe::with('appareils')->find($reclamation->equipe_id);

        $this->sendNotification($push,$equipe->appareils);
    }
}