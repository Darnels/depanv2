<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 12/06/2017
 * Time: 17:48
 */

namespace App\Http\Controllers\Users;


use App\Http\Controllers\Controller;
use App\Metier\UsersProcessing;
use App\Utilisateur;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    use UsersProcessing;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addUser(Request $request)
    {
        $this->validate($request,$this->validateRules());

        $collection = new Collection($this->prepareDataFromRequest($request));
        //check file input
        if($this->profilePictureCheck($request)){
            $collection->put('profile',$this->profileProcessing($collection->get('profile')));
        }

        $this->saveUser(new Utilisateur(),$collection);

        return back()->with('success',"Utilisateur créé avec succès !");
    }

    public function updateUser(Request $request, $email)
    {
        $this->validate($request,$this->validateRules());

        $collection = new Collection($this->prepareDataFromRequest($request));

        if(! $this->passwordPolicy($collection->get('password'))){
            return back()->withErrors("Votre mot de passe ne respecte pas les règles de complexité");
        }

        //check file input
        if($this->profilePictureCheck($request))
            $collection->put('profile',$this->profileProcessing($collection->get('profile')));

        $this->saveUser($this->findUserByEmail($this->decodeData($email)),$collection);

        return back()->with('success',"Utilisateur modifié");
    }
}