<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 12/06/2017
 * Time: 17:49
 */

namespace App\Http\Controllers\Users;


use App\Http\Controllers\Controller;
use App\Metier\Role;
use App\Metier\UsersProcessing;
use App\Utilisateur;
use Carbon\Carbon;

class UserController extends Controller
{
    use UsersProcessing;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showListUsers()
    {
        $roles = Role::getProfileTemplate();
        $users = Utilisateur::all();
        return view('users.liste',compact("users","roles"));
    }

    public function showUpdate($email)
    {
        $user = null;
        $bonCrees = 0;

        try
        {
            $user = $this->findUserByEmail($this->decodeData($email));

            //Nombre total de réclamations créées du mois en cours
            $bonCrees = $user->reclamations()
                ->whereBetween('datereclamation',[
                Carbon::now()->firstOfMonth()->toDateString(),
                Carbon::now()->toDateString(),
            ])->count();

        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

        $roles = Role::getProfileTemplate();

        return view('users.profile',compact("user","roles","bonCrees"));
    }

    public function showNewUserForm()
    {
        return view('users.add',compact());
    }
}