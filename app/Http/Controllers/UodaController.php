<?php

namespace App\Http\Controllers;

use App\Status;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UodaController extends Controller
{
    const Q0 = "00:00-23:59:59";
    const Q1 = "07:00-14:59:59";
    const Q2 = "15:00-22:59:59";
    const Q3 = "23:00-06:59:59";

    private $conditions;
    private $time;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showStatistiques(Request $request)
    {
        return $this->performView($request);
    }

    private function performView(Request $request)
    {
        $this->getInputField($request);

        $equipes = $this->getStatisticsEquipe();
        $secteurs = $this->getStatisticsExploitation();
        $zones = $this->getStatisticsDirection();

        $quart = new Collection([
            self::Q0 => "Toutes les heures",
            self::Q1 => "7h - 15h",
            self::Q2 => "15h - 23h",
            self::Q3 => "23h - 7h",
        ]);

        return view('uoda.stats-generales',compact("quart","equipes", "secteurs", "zones"));
    }

    private function getStatisticsEquipe()
    {
        return DB::table('equipe AS e')
            ->select(DB::raw("e.libelle,
              (SELECT count(r.id) FROM reclamation r WHERE e.id = r.equipe_id  AND 
              r.status = '".Status::RECLAMATION_TRANSFEREE_EQUIPE."' AND 
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS NON_EXEC,
            
              (SELECT count(r.id) FROM reclamation r WHERE e.id = r.equipe_id AND 
              r.status = '".Status::RECLAMATION_TRAITEE_TERMINE."' AND
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS DEPAN")
            )->get();
    }

    private function getStatisticsExploitation()
    {
        return DB::table('exploitation AS e')
            ->select(DB::raw("e.libelle,
              (SELECT count(r.id) FROM reclamation r WHERE e.id = r.exploitation_id  AND 
              r.status = '".Status::RECLAMATION_TRANSFEREE_EQUIPE."' AND 
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS NON_EXEC,
            
              (SELECT count(r.id) FROM reclamation r WHERE e.id = r.exploitation_id AND 
              r.status = '".Status::RECLAMATION_TRAITEE_TERMINE."' AND
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS DEPAN")
            )->get();
    }

    private function getStatisticsDirection()
    {
        return DB::table('direction AS d')
            ->select(DB::raw("d.libelle,
              (SELECT count(r.id) FROM reclamation r JOIN exploitation e ON e.id = r.exploitation_id
                WHERE e.direction_id = d.id AND r.status = '".Status::RECLAMATION_TRANSFEREE_EQUIPE."' AND 
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS NON_EXEC,
            
              (SELECT count(r.id) FROM reclamation r JOIN exploitation e ON e.id = r.exploitation_id 
                WHERE e.direction_id = d.id AND r.status = '".Status::RECLAMATION_TRAITEE_TERMINE."' AND
                (r.dateaffectation BETWEEN ".$this->conditions." OR r.dateaffectation IS null) ".$this->time." ) AS DEPAN")
            )->get();
    }

    private function getInputField(Request $request)
    {
        $data = new Collection([
            'h1' => '00:00:00',
            'h2' => '23:59:59',
            'd1' => Carbon::now()->firstOfMonth()->toDateString(),
            'd2' => Carbon::now()->toDateString()
        ]);

        if($request->has('q'))
        {
            $raw = explode("-",$request->input('q'));
            $data->put('h1',$raw[0]);
            $data->put('h2',$raw[1]);
        }

        if($request->has('periodedebut'))
            $data->put('d1', Carbon::createFromFormat('d/m/Y',$request->input('periodedebut'))->toDateString());

        if($request->has('periodefin'))
            $data->put('d2', Carbon::createFromFormat('d/m/Y',$request->input('periodefin'))->toDateString());

        $this->conditions = "'".implode(" ",[$data->get('d1'), $data->get('h1')])."' AND '".implode(" ",[$data->get('d2'), $data->get('h2')])."'";
        $this->time = " AND (time(r.dateaffectation) BETWEEN '".$data->get('h1')."' AND '".$data->get('h2')."') ";
    }
}