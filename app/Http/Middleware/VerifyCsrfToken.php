<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/client/recherche',
        '/devices/register/token', //Route de l'enregistrement des token et ANDROID_ID des tablettes
        '/devices/register/location', //Route de l'enregistrement des positions GPS des tablettes
        '/devices/rapport', //Route de récupération de rapport de tache de chaque tablette
        '/devices/distance/matrix', //Route de récupération des distance Google Matrix de chaque tablette pour une panne
        '/devices/depannage/status', //Route de MAJ des statuts de résolution d'un dépannage
        '/devices/auth/equipe', //Route de d'authentification des équipes
        '/devices/auth/equipe/logout', //Route de déconnexion des équipes
    ];

    public function handle($request, \Closure $next)
    {
        try{
            return parent::handle($request, $next);
        }catch (TokenMismatchException $e){
            return back()->withErrors(['Votre session a expirée. Veuillez recommencer SVP'])
                         ->withInput($request->except('_token'));
        }
    }
}