<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 25/02/2017
 * Time: 16:00
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class TypeReclamation extends Model
{
    protected $table = "typereclamation";
    public $timestamps = false;
    protected $guarded = [];
}