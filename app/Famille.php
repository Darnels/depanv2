<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Famille extends Model
{
    protected $table = "famille";
    protected $primaryKey = "code";
    public $timestamps = false;
    public $incrementing = false;
}
