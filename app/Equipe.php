<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $table = 'equipe';
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $hidden =  ['password'];

    public function appareils(){
        return $this->hasMany(Appareil::class,'equipe_id');
    }

    public function reclamations(){
        return $this->hasMany(Reclamation::class,'equipe_id');
    }

}
