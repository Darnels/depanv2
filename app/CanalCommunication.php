<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 25/02/2017
 * Time: 15:50
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CanalCommunication extends Model
{
    protected $table = "canalcommunication";
    public $timestamps = false;
    protected $guarded = [];
}