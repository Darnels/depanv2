<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 22/02/2017
 * Time: 08:08
 */

namespace App\Firebase;


class  FirebaseBase
{
    protected $error;
    const FIREBASE_API_KEY = "AAAAYJdSjsU:APA91bEfDR70-pPr1j1VGGYUzEEEcm2-avYr55NVfMiklBU6NvvIDpzCr5qopD6a2exbWiTbg_p-usmq7vc5Q_P0R9pyDUm1oyfLmj5sHVtID0sxjbA01IFlxV60uD3h8BAm1CrSiMIG";
    const URL_TO_SEND = "https://fcm.googleapis.com/fcm/send";

    protected function headers() {
        return [
            "Authorization: key=".self::FIREBASE_API_KEY,
            "Content-Type: application/json"
        ];
    }

    public function getErrorInfo() {
        return $this->error;
    }

    protected function sendPushNotification(array $fields) {

        $connexion = curl_init();

        curl_setopt($connexion,CURLOPT_URL,self::URL_TO_SEND);

        curl_setopt($connexion,CURLOPT_POST,true);
        curl_setopt($connexion,CURLOPT_POSTFIELDS,json_encode($fields));

        curl_setopt($connexion,CURLOPT_HTTPHEADER,$this->headers());
        curl_setopt($connexion,CURLOPT_RETURNTRANSFER,true);

        curl_setopt($connexion,CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($connexion);

        if($result === false){
            throw new FirebaseException(curl_error($connexion),FirebaseException::GOOGLE_API_ERROR);
        }

        return $result;
    }

    public function sendNotificationToOneDevice($deviceToken, Push $push){
        return $this->sendPushNotification(
            [
                "to" => $deviceToken,
                "data" => $push->getMessagePush()
            ]
        );
    }
}