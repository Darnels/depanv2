<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 25/02/2017
 * Time: 16:59
 */

namespace App;


class Status
{
    const RECLAMATION_CREEE = "000";
    const RECLAMATION_TRANSFEREE_EQUIPE = "001";
    const RECLAMATION_TRAITEE_TERMINE = "002";

    const UTILISATEUR_ACTIF = 1;
    const UTILISATEUR_INACTIF = 2;
    const UTILISATEUR_BLOQUE = 3;
    const UTILISATEUR_PROVISOIRE = 4;

    public static function getString($status){
        $msg =null;

        switch ($status){
            case self::RECLAMATION_CREEE : $msg = "Réclamation créée"; break;
            case self::RECLAMATION_TRANSFEREE_EQUIPE : $msg = "Transférée aux équipes"; break;
            case self::RECLAMATION_TRAITEE_TERMINE : $msg = "Résolu"; break;

            default: $msg = "Statut inconnu";
        }

        return $msg;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getUserStatusList()
    {
        return collect([
            "actif" => self::UTILISATEUR_ACTIF,
            "inactif" => self::UTILISATEUR_INACTIF,
            "bloque" => self::UTILISATEUR_BLOQUE,
            "provisoire" => self::UTILISATEUR_PROVISOIRE,
        ]);
    }
}