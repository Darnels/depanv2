<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 18/04/2017
 * Time: 13:03
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';
    protected $guarded = [];
    public $timestamps = false;
}