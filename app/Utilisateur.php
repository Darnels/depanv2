<?php

namespace App;

use App\Metier\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Utilisateur extends Authenticatable
{
    protected $table  = 'utilisateur';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'role' => 'array'
    ];

    public function reclamations(){
        return $this->hasMany(Reclamation::class);
    }

    public function passwordHistory(){
        return $this->hasMany(PasswordHistory::class);
    }

    public function checkRole($arg)
    {
        $roles = new Collection($this->role);
        $roles = $roles->union(Role::getProfileTemplate()->toArray());

        return $roles->get($arg);
    }
}
