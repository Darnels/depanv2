<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 24/02/2017
 * Time: 16:41
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $table = 'direction';
    protected $guarded = [];
    public $timestamps = false;
}