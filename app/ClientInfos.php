<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfos extends Model
{
    protected $table = 'clientinfos';
    protected $guarded = [];
    public $timestamps = false;
    protected $connection = "oldproduction";

    public function gps(){
        return $this->hasOne('App\GpsClient','gpsClientRef','clientRef');
    }
}
