<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsClient extends Model
{
    protected $table = "gpsclient";
    public $timestamps = false;
    protected  $guarded = [];
    protected $connection = "oldproduction";
}
