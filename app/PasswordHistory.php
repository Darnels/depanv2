<?php
/**
 * Created by PhpStorm.
 * User: SUPPORT.IT
 * Date: 01/04/2018
 * Time: 23:56
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class PasswordHistory extends Model
{
    protected $table = "password_history";
    public $timestamps = false;

    public function utilisateur(){
        return $this->belongsTo(Utilisateur::class);
    }
}