<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 19/02/2017
 * Time: 02:10
 */

namespace App\Metier;


use App\Appareil;
use App\Firebase\FirebaseBase;
use App\Firebase\Push;
use App\PositionAppareil;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

trait AppareilProcessing
{
    protected function getDeviceByAndroidID($androidID){
        return Appareil::with('equipe')->where('android_id',$androidID)->firstOrNew(['android_id' => $androidID]);
    }

    public function validateDeviceToken(Request $request){
        $this->validate($request,[
            "android_id" => "required",
            "token" => "required",
        ],[
            "android.required" => "L'identifaint de sécurité de l'appareil est requis.",
            "token.required" => "Le token est absent."
        ]);

        return $this;
    }

    private function sendNotification(Push $push, Collection $appareils)
    {
        foreach ($appareils as $appareil)
        {
            $fireBase = new FirebaseBase();
            $fireBase->sendNotificationToOneDevice($appareil->token,$push);
        }
    }
}