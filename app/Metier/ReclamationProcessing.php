<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 27/02/2017
 * Time: 17:33
 */

namespace App\Metier;


use App\Http\Controllers\Export\ExcelGenerate;
use App\Http\Controllers\Export\PdfGenerate;
use App\Reclamation;
use App\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

trait ReclamationProcessing
{
    use PdfGenerate, ExcelGenerate;

    protected function getMessageForPush(Reclamation $reclamation)
    {
        return "Le client ".$reclamation->client->nom." ".
        $reclamation->client->prenoms." a signalé un incident de type ".
        $reclamation->panne->libelle;
        //return $reclamation->commentaire;
    }
    protected function getMessageForLostReclamation(Reclamation $reclamation)
    {
        return "le bon ".$reclamation->numero." vous a été rétiré";
    }

    protected function getReclamationByNumber($numero)
    {
        $reclamation = Reclamation::with('client','equipe','canalCommunnication','panne','type','priorite','sensibilite','compterendu')
            ->where('numero',$numero)->get()->first();

        if ($reclamation == null)
            throw new ModelNotFoundException("Reclamation non trouvée");

        return $reclamation;
    }

    private function getReclamationEndNumero($numero){
        return Reclamation::with('client','equipe','canalCommunnication','panne','type','priorite','sensibilite','compterendu')
            ->where('numero','like',"%".$numero)->firstOrFail();
    }

    protected function getReclamations($conditions='=',$statut = Status::RECLAMATION_TRAITEE_TERMINE)
    {
        return $encours = Reclamation::with('client','panne','equipe')
            ->where('status',$conditions,$statut)
            ->orderBy('datereclamation','desc')
            ->orderBy('dateaffectation','desc')
            ->paginate(30);
    }

    private function sendViewListe($typeSatut, Request $request)
    {
        $datedebut = new Carbon();
        $datefin = new Carbon();
        if ($request->has('periodedebut') && $request->has('periodefin'))
        {
            $datedebut = Carbon::createFromFormat("d/m/Y", $request->input('periodedebut'));
            $datefin = Carbon::createFromFormat("d/m/Y", $request->input('periodefin'));
        }else {
            $datedebut = $datedebut->firstOfMonth();
        }

        $bons = $this->listeStatutBon($typeSatut, $datedebut, $datefin);

        if($request->query('print') == "pdf"){
            return $this->printListeReclamations($bons);
        }
        if($request->query('print') == "excel"){
            return $this->exportListeReclamation($bons);
        }

        return view('crc.etatliste_typebon', compact("bons"));
    }
}