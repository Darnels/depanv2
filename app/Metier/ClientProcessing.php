<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 17/02/2017
 * Time: 18:00
 */

namespace App\Metier;


use App\Client;
use Illuminate\Support\Facades\DB;

trait ClientProcessing
{
    protected function getClientByKeyword($searchText)
    {
        return Client::where(DB::raw('concat(nom," ",prenoms, " ",refbranch, " ",idabon, "  ",numctr)'),'like',"%$searchText%")
            ->select()
            ->paginate(30);
    }

    protected function showViewForMany($data)
    {
        $vue =  view('client.listesearch',[
            "clients" => $data,
            "criteres"=>$this->getListCritere()
        ]);

        if($data->count() == 0 ){
            $vue->withErrors('Client introuvable! Réessyez en saisissant le nom, les prénoms, la reference ou l\'identifiant de l\'abonné SVP');
        }

        return $vue;
    }

    /**
     * @return array
     */
    public function getListCritere(){
        return[
            "codexp" =>"Code Exploitation",
            "idabon" =>"Identifiant Abonné",
            "refbranch" => "Reference Branchement",
            "nom" => "Nom Abonné",
            "prenoms" => "Prénoms Abonné",
            "numctr" => "Numéro Compteur",
        ];
    }
}