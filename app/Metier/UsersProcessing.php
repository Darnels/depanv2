<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 12/06/2017
 * Time: 18:38
 */

namespace App\Metier;


use App\Notify\Notification;
use App\PasswordHistory;
use App\Status;
use App\Utilisateur;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

trait UsersProcessing
{
    public function findUserByEmail($email){
        $user = Utilisateur::where('email',$email)->firstOrNew([]);

        if(! $user)
            throw new ModelNotFoundException('Utilisateur introuvable');

        return $user;
    }

    protected function validateRules(){
        return [
            "name" => "required",
            "email" => "required|email",
            "password" => "required_if:changePassword,on|confirmed",
            "role" => "required|array",
            "statut" => "required|numeric|in:1,2,3,4"
        ];
    }

    protected function profilePictureCheck(Request $request)
    {
        if(Input::hasFile('profile'))
            return true;
        else
            return false;
    }

    protected function prepareDataFromRequest(Request $request)
    {
        $except = ['_token','changePassword','password_confirmation'];

        if(! $request->input('password'))
            $except[] = 'password';

        return $request->except($except);
    }

    /**
     * @param Utilisateur $utilisateur
     * @param Collection $data
     * @return $this
     * @throws \Throwable
     */
    protected function saveUser(Utilisateur $utilisateur, Collection $data)
    {
        if($data->has('password')){
            if(! $this->passwordPolicy($data->get('password'))){
                return back()->withErrors("Votre mot de passe ne respecte pas les règles de complexité");
            }
            $data->put('password',bcrypt($data->get('password')));
        }

        $utilisateur->guard([]);
        $utilisateur->forceFill($data->toArray());

        $utilisateur->saveOrFail();
    }

    protected function passwordPolicy($password)
    {
        $min = preg_match('/[a-z]+/', $password);
        $maj = preg_match('/[A-Z]+/', $password);
        $digit = preg_match('/[0-9]+/', $password);
        $punct = preg_match('/[[:punct:]]+/', $password);

        $all = ($min+$maj+$digit+$punct);

        if($all > 3 && strlen($password) > 7) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    protected function profileProcessing(UploadedFile $upload)
    {
        $upload->move(public_path(FileUpload::$PROFIL_DIRECTORY),$upload->getClientOriginalName());

        return $upload->getClientOriginalName();
    }

    public function showPasswordChange()
    {
        $user = Auth::user();
        return view("users.password", compact("user"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            "email" => "required|exists:utilisateur",
            "password" => "required|confirmed",
            "password_old" => "required",
        ]);

        try{
            $user = Utilisateur::where("email", $request->input("email"))->firstOrFail();

            $this->checkPassword($user, $request);

            //Sauvegarde du mot de passe
            $history = new PasswordHistory();
            $history->password = $user->password;
            $history->dhms_archive = Carbon::now()->toDateString();
            $history->utilisateur()->associate($user);
            $history->saveOrFail();

            $user->statut = Status::UTILISATEUR_ACTIF;
            $user->password = bcrypt($request->input("password"));
            $user->dhms_pwd_update = Carbon::now()->toDateString();
            $user->saveOrFail();

            return redirect()->route('accueil')->with(Notification::NOTIFICATION_KEY,"Votre mot de passe à été changé avec succès.");
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

    }

    /**
     * @param Utilisateur $user
     * @param Request $request
     * @throws \Exception
     */
    protected function checkPassword(Utilisateur $user, Request $request)
    {
        if(! $this->passwordPolicy($request->input("password")) )
        {
            throw new \Exception("Votre mot de passe ne respecte pas les règles de complexité");
        }

        //On check le mot de passe
        if($request->input("password_old") == $request->input("password")){
            throw new \Exception("Votre mot de passe est identique à l'ancien qui a expiré.");
        }

        //On boucle sur les ' derniers mot de passe
        foreach ($user->passwordHistory()->orderBy("dhms_archive", "desc")->limit(4)->get() as $oldPwd)
        {
            if(Hash::check($request->input("password"), $oldPwd->password)){
                throw new \Exception("Votre mot de passe est identique à l'un de vos 4 derniers mot de passe expiré.");
            }
        }
    }
}