<?php
/**
 * Created by PhpStorm.
 * User: SUPPORT.IT
 * Date: 03/04/2018
 * Time: 19:32
 */

namespace App\Metier;


use Illuminate\Support\Facades\Storage;

class AccessLog
{
    const FILE_NAME = "access.log";
    const DISK = 'log';
    const ARCHIVE_DIRECTORY = "/archive";

    public static function writeData($message)
    {
        if(!AccessLog::logFileExist()){
            self::makeLogFile();
        }

        $size = Storage::disk(self::DISK)->size(self::FILE_NAME);

        //1024 * 1 => Ko
        //1024 * 2 => Mo
        //1024 * 3 => Go
        //1024 * 4 => To
        if($size >= intval(env("LOG_MAX_SIZE",5) * pow(1024,2)))
        {
            self::archiveLogFile();
        }

        Storage::disk(self::DISK)->prepend(self::FILE_NAME,
            sprintf('[%s]  %s',date('Y-m-d H:i:s'), $message)
        );
    }

    protected static function archiveLogFile()
    {
        Storage::disk(self::DISK)->makeDirectory(self::ARCHIVE_DIRECTORY);

        Storage::disk(self::DISK)
            ->move(self::FILE_NAME, self::ARCHIVE_DIRECTORY.'/'.date('YmdHis').'_'.self::FILE_NAME);
    }

    /**
     * @return mixed
     */
    protected static function logFileExist()
    {
        return Storage::disk(self::DISK)->exists(self::FILE_NAME);
    }

    protected static function makeLogFile()
    {
        Storage::disk(self::DISK)->put(self::FILE_NAME, null);
    }
}