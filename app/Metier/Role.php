<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 12/06/2017
 * Time: 20:07
 */

namespace App\Metier;


use Illuminate\Database\Eloquent\Collection;

class Role
{
    const AGENT_CRC = 'agent-crc';
    const CHARGE_CRC = 'charge-crc';
    const RESPONSABLE_CRC = 'resp-crc';
    const RESPONSABLE_UODA = 'resp-uoda';
    const DIRECTEUR = 'directeur';
    const ADMINISTRATEUR = 'admin';

    /**
     * @return Collection
     */
    public static function getProfileTemplate()
    {
        return new Collection([
            self::ADMINISTRATEUR => 0,
            self::DIRECTEUR => 0,
            self::AGENT_CRC => 0,
            self::CHARGE_CRC => 0,
            self::RESPONSABLE_CRC => 0,
            self::RESPONSABLE_UODA => 0,
        ]);
    }

    public static function getProfileFullName($key)
    {
       $table = [
           self::ADMINISTRATEUR => "ADMINISTRATEUR",
           self::DIRECTEUR => "DIRECTEUR",
           self::AGENT_CRC => "AGENT CRC",
           self::CHARGE_CRC => "CHARGE CRC",
           self::RESPONSABLE_CRC => "RESPONSABLE CRC",
           self::RESPONSABLE_UODA => "RESPONSABLE UODA",
       ];
       return $table[$key];
    }
}