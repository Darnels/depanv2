<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appareil extends Model
{
    protected $table = 'appareil';
    protected $guarded = [];
    public $timestamps = false;

    public function positions(){
        return $this->hasMany('App\PositionAppareil','appareil_id');
    }

    public function equipe(){
        return $this->belongsTo(Equipe::class,'equipe_id');
    }
}