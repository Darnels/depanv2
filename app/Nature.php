<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nature extends Model
{
    protected $table = "nature";

    public $timestamps = false;

    public function famille(){
        return $this->belongsTo(Famille::class);
    }
}
