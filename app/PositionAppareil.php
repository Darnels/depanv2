<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 26/02/2017
 * Time: 12:21
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class PositionAppareil extends  Model
{
    protected $table = "positionappareil";
    protected $guarded = ['id'];
    public $timestamps = false;

    public function appareil(){
        return $this->belongsTo('App\Appareil','appareil_id');
    }

    public function getLatestPosition(){
        return PositionAppareil::with('appareil')->where('appareil_id',$this->appareil_id)->latest('dateposition')->get();
    }
}