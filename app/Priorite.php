<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 09/03/2017
 * Time: 17:42
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Priorite extends Model
{
    const PRIORITE_TRES_URGENT = 1;
    const PRIORITE_URGENT = 2;
    const PRIORITE_NORMAL = 3;

    public $timestamps = false;
    protected $table = 'priorite';
}