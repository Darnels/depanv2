<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompteRendu extends Model
{
    const MAI = "MAI";
    const DEF = "DEF";
    const PRO = "PRO";

    protected $table = "compterendu";
    public $timestamps = false;

    public function nature(){
        return $this->belongsTo(Nature::class);
    }

    public function reclamation(){
        return $this->belongsTo(Reclamation::class);
    }
}
