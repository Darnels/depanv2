<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 24/02/2017
 * Time: 17:22
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Reclamation extends Model
{
    public $timestamps = false;
    protected $table = 'reclamation';
    protected $guarded = [];

    public function client(){
        return $this->belongsTo(Client::class,'client_id','id');
    }

    public function canalCommunnication(){
        return $this->belongsTo(CanalCommunication::class,'canalcommunication_id');
    }

    public function equipe(){
        return $this->belongsTo(Equipe::class,'equipe_id');
    }

    public function panne(){
        return $this->belongsTo(Panne::class,'panne_id');
    }

    public function exploitation(){
        return $this->belongsTo(Exploitation::class,'exploitation_id');
    }

    public function type(){
        return $this->belongsTo(TypeReclamation::class,'typereclamation_id');
    }

    public function priorite(){
        return $this->belongsTo(Priorite::class,'priorite_id');
    }

    public function sensibilite(){
        return $this->belongsTo(Sensibilite::class,'sensibilite_id');
    }

    public function compterendu(){
        return $this->hasOne(CompteRendu::class,'reclamation_id');
    }

    protected $casts = [
        "nonabonnne" => 'boolean',
        "relance" => 'boolean',
        "depanageenligne" => 'boolean',
    ];
}