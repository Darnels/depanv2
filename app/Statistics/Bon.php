<?php
/**
 * Created by PhpStorm.
 * User: Darnels
 * Date: 07/06/2017
 * Time: 21:32
 */

namespace App\Statistics;


use App\Reclamation;
use App\Status;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait Bon
{
    private function statistiqueBonByState()
    {
        return DB::select(DB::raw('SELECT 
        (SELECT COUNT(id) FROM `reclamation` WHERE status = "'.Status::RECLAMATION_TRANSFEREE_EQUIPE.'" or status = "'.Status::RECLAMATION_TRAITEE_TERMINE.'") as "transmis" , 
        (SELECT COUNT(id) FROM `reclamation` WHERE status = "'.Status::RECLAMATION_TRAITEE_TERMINE.'") as "executes",
        (SELECT SEC_TO_TIME(AVG(time_to_sec(timediff(datefinintervention, datereclamation)))) from reclamation) moyenne'));
    }

    private function listBonByCall(Carbon $datedebut, Carbon $datefin)
    {
       return Reclamation::with('canalCommunnication','panne','exploitation','type','priorite','client')
       ->whereBetween("datereclamation", [$datedebut->toDateString(),$datefin->toDateString()])
        ->paginate(10);
    }

    private function listeStatutBon($typeStatus,$debut = null, $fin = null)
    {
        return DB::select(DB::raw ("SELECT reclamation.numero, compterendu.statut,  client.nom, client.prenoms, reclamation.datereclamation, reclamation.nomappelant,   
                                    panne.libelle as pannelib, priorite.libelle as prioritelib, exploitation.libelle as exploitationlib, direction.libelle
                                  FROM `reclamation`,`compterendu`,`client`,`panne`,`exploitation`,`priorite`,`direction` WHERE compterendu.statut='$typeStatus'
                                  AND reclamation.id = compterendu.reclamation_id AND reclamation.client_id = client.id AND reclamation.panne_id = panne.id
                                  AND reclamation.exploitation_id = exploitation.id AND priorite.id = reclamation.priorite_id AND direction.id = exploitation.direction_id"));

    }

    private function statistiqueBonByAffect()
    {
        return DB::select(DB::raw('SELECT 
        (SELECT COUNT(reclamation.id) FROM `reclamation`,`compterendu` WHERE compterendu.statut="MAI" AND reclamation.id = compterendu.reclamation_id) as maintenance,
        (SELECT COUNT(reclamation.id) FROM `reclamation`,`compterendu` WHERE compterendu.statut="PRO" AND reclamation.id = compterendu.reclamation_id) as provisoire,
        (SELECT COUNT(reclamation.id) FROM `reclamation`,`compterendu` WHERE compterendu.statut="DEF" AND reclamation.id = compterendu.reclamation_id) as definitif
        
        '));
    }
}

