<?php
/**
 * Created by PhpStorm.
 * User: BW.KOFFI
 * Date: 07/06/2017
 * Time: 19:36
 */

namespace App\Statistics;


use App\CompteRendu;
use App\Famille;
use App\Nature;
use Illuminate\Support\Facades\DB;

trait Depannage
{
    private function statistiquesByFamilly()
    {
        return CompteRendu::join('nature','compterendu.nature_id','=','nature.id')
            ->rightJoin('famille','famille.code','=','nature.famille_code')
            ->groupBy('libelle')
            ->select(DB::raw('famille.libelle, count(compterendu.id) as total'))
            ->get();
    }

    private function statistiquesByNature($famille)
    {
        return CompteRendu::rightJoin('nature','compterendu.nature_id','=','nature.id')
            ->join('famille','famille.code','=','nature.famille_code')
            ->where('famille.code',$famille)
            ->groupBy('libelle')
            ->select(DB::raw('nature.libelle, count(compterendu.id) as total'))
            ->get();
    }
}