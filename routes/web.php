<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login'); //->middleware('throttle:3,1');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/accueil', 'HomeController@index')->name('accueil');

//Les clients
Route::get('/client/recherche','SearchClientController@searchClient')->name('recherche_client');

//Les réclamations
//Route::post('/reclamation/recherche/client','SearchClientController@searchClient')->name('recherche_client');
Route::get('/reclamation/nouvelle/{referenceClient}','ReclamationController@showNewReclamation')->name('new_reclamation');
Route::get('/reclamation/liste/{referenceClient}','ReclamationController@showListeReclamation')->name('liste_reclamation');
Route::post('/reclamation/nouvelle/{referenceClient}','ReclamationController@saveReclamation');
Route::get('/reclamation/modification/{reclamationNumber}','ReclamationController@showUpdateReclamation')->name('up_reclamation');
Route::post('/reclamation/modification/{reclamationNumber}','ReclamationController@updateReclamation');
Route::get('/reclamation/{reclamationNumber}/plannification','DispatchingController@showMapForReclamation')->name('plannification_bt');
Route::post('/reclamation/{reclamationNumber}/plannification','DispatchingController@assignReclamationToDevice');
Route::get('/reclamation/{reclamationNumber}/details','ReclamationController@showDetailReclamation')->name('reclamation_details');
Route::post('/reclamation/{reclamationNumber}/details','ReclamationController@detailUpReclamation');
Route::get('/reclamations/encours','ReclamationController@showListEncours')->name('reclamation_encours');
Route::get('/reclamations/terminees','ReclamationController@showListClose')->name('reclamation_terminees');
Route::get('/reclamations/attentes','ReclamationController@showListWait')->name('reclamation_nonaffectees');

Route::get('/cartographie/equipes','MapController@showMapDevice')->name('carte');
Route::get('/cartographie/devices/localisation','DeviceController@getLatestPositionFromAll')->name('devices_positions');
Route::get('/{device}/auth/taches','DeviceController@getTachesByDevicesAuth')->name('liste_tache_auth');
Route::post('/devices/distance/matrix','DeviceController@getGoogleDistanceMatrixInfos')->name('distance_matrix');

//les equipements ou Appareils
Route::group(["prefix" => "appareils"],function () {
    Route::get('/', 'AppareilController@showListeEquipe')->name('liste_appareils');
    Route::get('/{androidID}/profil', 'AppareilController@showAppareilProfil')->name('appareil_profil');
    Route::post('/{androidID}/profil', 'AppareilController@updateAppareil');
});

//Routes des statistiques
Route::group(["prefix" => "statistiques"],function (){
    Route::get('/uoda',"UodaController@showStatistiques")->name('stat_uoda');
    Route::get('/crc/etatmensuel','CrcController@showEtatReclamation')->name('rapport_mensuel');
    Route::get('/crc/nature/famille/{code}','CrcController@ajaxNatures')->name('ajax_famille_nature');
    Route::get('/crc/etat/appels','ReclamationController@showListDate')->name('rapport_appel');
    Route::get('/crc/etat/provisoire','ReclamationController@showListPro')->name('rapport_pro');
    Route::get('/crc/etat/maintenance','ReclamationController@showListMai')->name('rapport_mai');
    Route::get('/crc/etat/definitif','ReclamationController@showListDef')->name('rapport_def');
});

//Gestion des utilisateurs
Route::group(["prefix" => "utilisateurs"],function (){
    Route::get('/','Users\UserController@showListUsers')->name('user_liste');
    Route::post('/','Users\RegisterController@addUser');
    Route::get('/details/{email}','Users\UserController@showUpdate')->name('user_profile');
    Route::post('/details/{email}','Users\RegisterController@updateUser');
    Route::get('/password/change','Users\RegisterController@showPasswordChange')->name("change_password");
    Route::post('/password/change','Users\RegisterController@changePassword');
});

//Gestion des Equipes
Route::group(["prefix" => "equipes"],function (){
    Route::get('/','Team\RegisterController@showListe')->name('equipe_liste');
    Route::post('/','Team\RegisterController@register');
    Route::get('/suppression/{token}','Team\DeleteController@showDeleteForm')->name("equipe_delete");
    Route::post('/suppression/{token}','Team\DeleteController@delete');
    Route::get('/{equipe}/profil','Team\ProfileController@showEquipeProfil')->name('equipe_profil');
    Route::post('/{equipe}/profil','Team\UpdateController@updateEquipe');
    Route::post('/changepassword','Team\UpdateController@updatePassword')->name("equipe_change_pwd");
});

//Home
Route::get('/accueil/depannages','HomeController@showDepannagesInfos')->name('liste_depannages');

//Les Devices //Groupe non authentifié
Route::group([ 'middleware' => 'guest' , 'prefix' => '/devices' ], function () {
    Route::post('/register/token','DeviceController@registerToken')->name('add_device');
    Route::post('/rapport','DeviceController@addReport')->name('add_rapport');
    Route::post('/register/location','DeviceController@registerLocation')->name('add_location');
    Route::get('/{equipe}/taches/{lastID?}','DeviceController@getTacheByDevice')->name('liste_tache');
    Route::post('/depannage/status','DeviceController@updateReclamationStatus')->name('depannage_status');
    Route::get('/famille','DeviceController@getFamille')->name('famille_nature');
    Route::get('/nature','DeviceController@getNatureByFamilly')->name('nature_familly');
    Route::get('/equipes','DeviceController@getTeam')->name('device_equipe');
    Route::post('/auth/equipe','DeviceController@authEquipe');
    Route::post('/auth/equipe/logout','DeviceController@logoutEquipe');
});