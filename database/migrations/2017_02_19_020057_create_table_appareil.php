<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAppareil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilisateur', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',100)->unique();
            $table->string('password');
            $table->date('dhms_pwd_update');
            $table->date('dhms_pwd_expire');
            //1=> Actif, 2=> bloqué 3=>provisoire, 4=>Provisoire
            $table->smallInteger('statut')->default(1);
            $table->date("expire_at")->nullable();
            $table->string('role')->nullable();
            $table->string('profile')->default('user-default.png');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('password_history',function (Blueprint $table){
            $table->increments('id');
            $table->string('password');
            $table->dateTime('dhms_archive');
            $table->unsignedInteger('utilisateur_id');
            $table->foreign('utilisateur_id')->references('id')->on('utilisateur');
        });

        Schema::create('equipe',function (Blueprint $table){
            $table->increments('id');
            $table->string('login')->unique();
            $table->string('password');
            $table->string('libelle',100);
            $table->dateTime('datecreation');
        });

        Schema::create('appareil',function (Blueprint $table){
            $table->increments('id');
            $table->text('token');
            $table->string('android_id')->unique();
            $table->string('libelle',100);
            $table->dateTime('dateconnexion');
            $table->unsignedInteger('equipe_id')->nullable();
            $table->foreign('equipe_id')->references('id')->on('equipe');
        });


        Schema::create('panne',function (Blueprint $table){
            $table->increments('id');
            $table->string('code',5)->unique();
            $table->string('libelle');
        });

        Schema::create('direction',function (Blueprint $table){
            $table->increments('id');
            $table->string('code',5)->unique();
            $table->string('libelle');
        });

        Schema::create('exploitation',function (Blueprint $table){
            $table->increments('id');
            $table->string('code',5)->unique();
            $table->string('libelle');
            $table->integer('direction_id')->unsigned();
            $table->foreign('direction_id')->references('id')->on('direction');
        });

        Schema::create('typedefaut',function (Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('nature');
        });

        Schema::create('materiel',function (Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('libelle');
            $table->string('stockminimum');
            $table->string('unite');
        });

        Schema::create('canalcommunication',function (Blueprint $table){
            $table->increments('id');
            $table->string('libelle');
        });

        Schema::create('typereclamation',function (Blueprint $table){
            $table->increments('id');
            $table->string('code',10)->unique();
            $table->string('libelle',30);
        });

        Schema::create('priorite',function (Blueprint $table){
            $table->increments('id');
            $table->string('libelle',50);
        });

        Schema::create('sensibilite',function (Blueprint $table){
            $table->increments('id');
            $table->string('libelle',50);
        });

        Schema::create('reclamation',function (Blueprint $table){
            $table->increments('id');
            $table->string("numero",100)->unique();
            $table->string('localisation');
            $table->dateTime('datereclamation');
            $table->dateTime('dateaffectation')->nullable();
            $table->dateTime('datedemarragenavigation')->nullable();
            $table->dateTime('dateheurefinnavigation')->nullable();
            $table->dateTime('datedebutintervention')->nullable();
            $table->dateTime('datefinintervention')->nullable();
            $table->text('rapport')->nullable();
            $table->boolean('nonabonne')->default(false);
            $table->string('nomappelant',150)->nullable();
            $table->string('contactappelant',60)->nullable();
            $table->unsignedInteger('equipe_id')->nullable();
            $table->string('status',3)->default('000');
            $table->integer('canalcommunication_id')->unsigned();
            $table->integer('panne_id')->unsigned();
            $table->integer('typereclamation_id')->unsigned();
            $table->integer('exploitation_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('priorite_id')->unsigned();
            $table->boolean('depanageenligne')->default(false);
            $table->text('commentaire')->nullable();
            $table->text('rapportcrc')->nullable();
            $table->integer('utilisateur_id')->unsigned();
            $table->integer('sensibilite_id')->unsigned()->default(\App\Sensibilite::NORMALE);
            $table->boolean('relance')->default(false);
            $table->dateTime('dateheurerelance')->nullable();
            $table->dateTime('datereaffectation')->nullable();
            $table->unsignedInteger('oldequipe_id')->nullable();
            $table->foreign('sensibilite_id')->references('id')->on('sensibilite');
            $table->foreign('utilisateur_id')->references('id')->on('utilisateur');
            $table->foreign('canalcommunication_id')->references('id')->on('canalcommunication');
            $table->foreign('panne_id')->references('id')->on('panne');
            $table->foreign('exploitation_id')->references('id')->on('exploitation');
            $table->foreign('typereclamation_id')->references('id')->on('typereclamation');
            $table->foreign('priorite_id')->references('id')->on('priorite');
            $table->foreign("equipe_id")->references('id')->on('equipe');
            $table->foreign("oldequipe_id")->references('id')->on('equipe');
        });

        Schema::create('positionappareil',function (Blueprint $table){
            $table->increments('id');
            $table->dateTime('dateposition');
            $table->string('latitude',50);
            $table->string('longitude',50);
            $table->string('speed',15);
            $table->integer('appareil_id')->unsigned();
            $table->foreign('appareil_id')->references('id')->on('appareil');
        });

        Schema::create('client',function (Blueprint $table){
            $table->increments('id');
            $table->string('codexp',10);
            $table->string('idabon',15)->unique();
            $table->string('refbranch',20);
            $table->string('nom');
            $table->string('prenoms')->nullable();
            $table->string('numctr',20);
            $table->string('posabon',5);
            $table->string('psabon',5);
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
        });

        Schema::create("famille",function (Blueprint $table){
            $table->string('code',1);
            $table->string('libelle');
            $table->primary("code");
        });

        Schema::create("nature",function (Blueprint $table){
            $table->increments("id");
            $table->string("libelle");
            $table->unsignedInteger("rang");
            $table->string("famille_code");
            $table->foreign("famille_code")->references("code")->on("famille");
        });

        Schema::create("compterendu",function (Blueprint $table){
            $table->increments("id");
            $table->dateTime("dateheurecontact");
            $table->dateTime("dateheuredebutintervention");
            $table->dateTime("dateheurefinintervention");
            $table->text("detailrapport");
            $table->string("statut");
            $table->string("assistancexterne")->nullable();
            $table->dateTime("dateheuredemande")->nullable();
            $table->dateTime("dateheurearrivee")->nullable();
            $table->unsignedInteger("nature_id");
            $table->unsignedInteger("reclamation_id");
            $table->unsignedInteger("equipe_id");
            $table->foreign("reclamation_id")->references('id')->on('reclamation');
            $table->foreign("nature_id")->references('id')->on('nature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compterendu');
        Schema::dropIfExists('nature');
        Schema::dropIfExists('famille');
        Schema::dropIfExists('reclamation');
        Schema::dropIfExists('positionappareil');
        Schema::dropIfExists('exploitation');
        Schema::dropIfExists('direction');
        Schema::dropIfExists('canalcommunication');
        Schema::dropIfExists('typedefaut');
        Schema::dropIfExists('typereclamation');
        Schema::dropIfExists('materiel');
        Schema::dropIfExists('appareil');
        Schema::dropIfExists('sensibilite');
        Schema::dropIfExists('panne');
        Schema::dropIfExists('typereclamation');
        Schema::dropIfExists('priorite');
        Schema::dropIfExists('client');
        Schema::dropIfExists('equipe');
        Schema::dropIfExists('password_history');
        Schema::dropIfExists('utilisateur');
    }
}