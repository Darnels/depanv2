<?php

use Illuminate\Database\Seeder;

class NatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("famille")->insert([
            [ "code" => "A", "libelle" => "Rapport en attente" ],
            [ "code" => "B", "libelle" => "Autres cas Défauts Réseaux & Postes HTA/BTA/Incidents HTB" ],
            //[ "code" => "C", "libelle" => "" ],
            [ "code" => "D", "libelle" => "Autres cas Défauts Réseaux BT Aériens Isolés & Souterrains" ],
            [ "code" => "E", "libelle" => "Autres Cas de Défauts Aériens Nus" ],
            [ "code" => "F", "libelle" => "Autres Cas de Défauts Souterrains" ],
            [ "code" => "G", "libelle" => "Autres Cas de Défauts Branchement Aérien" ],
            [ "code" => "H", "libelle" => "Autres Cas de Défauts de Masse" ],
            [ "code" => "I", "libelle" => "Autres Cas de Défauts Tableau de Compage" ],
            [ "code" => "J", "libelle" => "Autres Cas de Défauts Installation Client" ],
            [ "code" => "K", "libelle" => "Autres Cas de Défauts Eclairage Public (EP)" ],
            [ "code" => "L", "libelle" => "Autres Cas d'Incendie" ],
            [ "code" => "M", "libelle" => "Autres Cas de Client pas vu, pas trouvé ou pas au RDV" ],
        ]);

        DB::table("nature")->insert([
            [
                "libelle" => "Bon mal saisi dans la base de données",
                "rang" => 1 ,
                "famille_code" => "A"
            ],
            [
                "libelle" => "Bon saisi dans la base de données sans rapport d'intervention",
                "rang" => 2,
                "famille_code" => "A"
            ],


            [
                "libelle" => "TUR, DU, T4-800 en court-circuit",
                "rang" => 1 ,
                "famille_code" => "B"
            ],
            [
                "libelle" => "TUR, DU, T4-800 brûlé",
                "rang" => 2 ,
                "famille_code" => "B"
            ],
            [
                "libelle" => "TUR, DU, T4-800 défectueux",
                "rang" => 3 ,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Défaut mécanique (TUR,DU,T4-800)",
                "rang" => 4,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Défaut transformateur (Borne BT en défaut, fuite d'huile",
                "rang" => 5 ,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Transformateur Avarié",
                "rang" => 6 ,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Poste HTA/BTA consigné",
                "rang" => 7,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Disjoncteur H61 défectueux",
                "rang" => 8,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Disjoncteur H61 en court-circuit",
                "rang" => 9,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Disjoncteur H61 brûlé",
                "rang" => 10,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Défaut disjoncteur (disjoncteur HP, tringlerie)",
                "rang" => 11,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Disjoncteur HP déclenché",
                "rang" => 12,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Câble BT, liaison Transformateur/TUR en défaut",
                "rang" => 13,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Câble BT, liaison Transformateur/TUR en court-circuit",
                "rang" => 14,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Câble BT, liaison Transformateur/TUR brûlé",
                "rang" => 15,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Câble BT, liaison Transformateur/TUR volé",
                "rang" => 16,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Tableau BTA brûlé",
                "rang" => 17,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Fusible HPC fondu",
                "rang" => 18,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Fusible HPC décroché",
                "rang" => 19,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Défaut de tension générale (Manque \"U\", Déclenchement, Panne secteur)",
                "rang" => 20,
                "famille_code" => "B"
            ],
            [
                "libelle" => "Surtension",
                "rang" => 21,
                "famille_code" => "B"
            ],[
                "libelle" => "Variation de tension générale dans la zone",
                "rang" => 22,
                "famille_code" => "B"
            ],[
                "libelle" => "Baisse de tension générale dans la zone",
                "rang" => 23,
                "famille_code" => "B"
            ],[
                "libelle" => "Courant sur le neutre en amont du TUR",
                "rang" => 24,
                "famille_code" => "B"
            ],[
                "libelle" => "Défaut compact",
                "rang" => 25,
                "famille_code" => "B"
            ],


            //[ "libelle" => "", "famille_code" => "C" ],

            [
                "libelle" => "Câble en fusion (Réseau préassemblé)",
                "rang" => 1,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble coupé",
                "rang" => 2,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble en court-circuit",
                "rang" => 3,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble à terre",
                "rang" => 4,
                "famille_code" => "D"
            ],[
                "libelle" => "Neutre du réseau coupé",
                "rang" => 5,
                "famille_code" => "D"
            ],[
                "libelle" => "Départ en défaut (Sortie Poste)",
                "rang" => 6,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT aérien, sortie poste en défaut",
                "rang" => 7,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT aérien, sortie poste en court-circuit",
                "rang" => 8,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT aérien, sortie poste brûlé",
                "rang" => 9,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT souterrain, sortie poste en défaut",
                "rang" => 10,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT souterrain, sortie poste en court-circuit",
                "rang" => 11,
                "famille_code" => "D"
            ],[
                "libelle" => "Câble BT souterrain, sortie poste brûlé",
                "rang" => 12,
                "famille_code" => "D"
            ],[
                "libelle" => "Défaut support béton",
                "rang" => 13,
                "famille_code" => "D"
            ],[
                "libelle" => "Défaut support bois",
                "rang" => 14,
                "famille_code" => "D"
            ],[
                "libelle" => "Support béton (percuté, cassé, déchaussé)",
                "rang" => 15,
                "famille_code" => "D"
            ],[
                "libelle" => "Support bois (percuté, cassé, déchaussé)",
                "rang" => 16,
                "famille_code" => "D"
            ],[
                "libelle" => "Défaut Potelet",
                "rang" => 17,
                "famille_code" => "D"
            ],[
                "libelle" => "Potelet cassé",
                "rang" => 18,
                "famille_code" => "D"
            ],[
                "libelle" => "Défaut en jonction aérien (manchonnage)",
                "rang" => 19,
                "famille_code" => "D"
            ],[
                "libelle" => "Défaut pince d'ancrage",
                "rang" => 20,
                "famille_code" => "D"
            ],[
                "libelle" => "Raccord du réseau BT oxydé",
                "rang" => 21,
                "famille_code" => "D"
            ],[
                "libelle" => "Raccord du réseau BT brûlé",
                "rang" => 22,
                "famille_code" => "D"
            ],[
                "libelle" => "Courant sur le neutre du réseau",
                "rang" => 23,
                "famille_code" => "D"
            ],


            [
                "libelle" => "Conducteur cassé",
                "rang" => 1,
                "famille_code" => "E"
            ],[
                "libelle" => "Conducteurs entremêlés",
                "rang" => 2,
                "famille_code" => "E"
            ],[
                "libelle" => "Branche sur ligne",
                "rang" => 3,
                "famille_code" => "E"
            ],[
                "libelle" => "Rupture de jonction,raccords",
                "rang" => 4,
                "famille_code" => "E"
            ],[
                "libelle" => "Isolateur BT (cassé, fêlé)",
                "rang" => 5,
                "famille_code" => "E"
            ],[
                "libelle" => "Corps étrangé sur la ligne",
                "rang" => 6,
                "famille_code" => "E"
            ],


            [
                "libelle" => "Câble en défaut (oxydé,brûlé, en court-circuit)",
                "rang" => 1,
                "famille_code" => "F"
            ],[
                "libelle" => "Câble pioché",
                "rang" => 2,
                "famille_code" => "F"
            ],[
                "libelle" => "Défaut de jonction souterrain (boîte de jonction, d'extrémité)",
                "rang" => 3,
                "famille_code" => "F"
            ],[
                "libelle" => "Défaut sur borne de lotissement (défectueux, brûlée, en court-circuit, bornes oxydées)",
                "rang" => 4,
                "famille_code" => "F"
            ],[
                "libelle" => "Défaut paninter, soclinter",
                "rang" => 5,
                "famille_code" => "F"
            ],

            /*

            [ "libelle" => "", "famille_code" => "G" ],

            [ "libelle" => "", "famille_code" => "H" ],

            [ "libelle" => "", "famille_code" => "I" ],

            [ "libelle" => "", "famille_code" => "J" ],

            [ "libelle" => "", "famille_code" => "K" ],

            [ "libelle" => "", "famille_code" => "L" ],

            [ "libelle" => "", "famille_code" => "M" ],

            */
        ]);
    }
}
