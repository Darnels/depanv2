<?php

use Illuminate\Database\Seeder;

class Datatesting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('utilisateur')->insert([
            [
                'email' => 'glamolondon@gmail.com',
                'name' => 'Will Koffi',
                'password' => bcrypt('flavie'),
                'dhms_pwd_update' => \Carbon\Carbon::now()->toDateString(),
                'dhms_pwd_expire' => \Carbon\Carbon::now()->addMonth(3)->toDateString(),
                'role' => json_encode(['admin' => "1"])
            ],[
                'email' => 'jeansidoineake@yahoo.fr',
                'name' => 'Sidoine Aké',
                'password' => bcrypt('leandre'),
                'dhms_pwd_update' => \Carbon\Carbon::now()->toDateString(),
                'dhms_pwd_expire' => \Carbon\Carbon::now()->addMonth(3)->toDateString(),
                'role' => json_encode(['admin' => "1"])
            ],
        ]);

        DB::table('equipe')->insert([
            [
                'id' => 1,
                'login' => 'equipe1',
                'libelle' => 'Equipe 1',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-20)->toDateTimeString(),
            ],[
                'id' => 2,
                'login' => 'equipe2',
                'libelle' => 'Equipe 2',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-26)->toDateTimeString(),
            ],[
                'id' => 3,
                'login' => 'equipe3',
                'libelle' => 'Equipe 3',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-32)->toDateTimeString(),
            ],[
                'id' => 8,
                'login' => 'equipe8',
                'libelle' => 'Equipe 8',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-15)->toDateTimeString(),
            ],[
                'id' => 18,
                'login' => 'equipe18',
                'libelle' => 'Equipe 18',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-12)->toDateTimeString(),
            ],[
                'id' => 19,
                'login' => 'equipe19',
                'libelle' => 'Equipe 19',
                'password' => bcrypt('azerty'),
                'datecreation' => \Carbon\Carbon::now()->addDays(-36)->toDateTimeString(),
            ],
        ]);


        DB::table('sensibilite')->insert([
            ['libelle' => 'Très sensible'],
            ['libelle' => 'Sensible'],
            ['libelle' => 'Normale'],
        ]);

        DB::table('priorite')->insert([
            ['libelle' => 'Très urgent'],
            ['libelle' => 'Urgent'],
            ['libelle' => 'Normale'],
        ]);

        DB::table('typereclamation')->insert([
            [ "code" => "BT" , "libelle" => "Dépannage BT" ],
            [ "code" => "PRO/BT" , "libelle" => "Provisoire BT" ],
            [ "code" => "MAI/BT" , "libelle" => "Maintenance BT" ],
        ]);

        DB::table('canalcommunication')->insert([
            [ "libelle" => "Téléphone" ],
            [ "libelle" => "Visite physique" ],
            [ "libelle" => "SMS" ],
            [ "libelle" => "Email" ],
            [ "libelle" => "Courrier" ],
        ]);

        DB::table('panne')->insert([
            [ "code" => "1", "libelle" => "Panne secteur" ],
            [ "code" => "10", "libelle" => "Potelet à masse" ],
            [ "code" => "11", "libelle" => "Maison à la masse" ],
            [ "code" => "12", "libelle" => "Mur à la masse" ],
            [ "code" => "13", "libelle" => "Toitures à la masse" ],
            [ "code" => "14", "libelle" => "Portail à la masse" ],
            [ "code" => "15", "libelle" => "Séchoir à la masse" ],
            [ "code" => "16", "libelle" => "Robinet à la masse" ],
            [ "code" => "17", "libelle" => "Problème au disjoncteur" ],
            [ "code" => "18", "libelle" => "Demande de pose disjoncteur" ],
            [ "code" => "19", "libelle" => "Demande de rétablissement suite coupure pour impayé" ],
        ]);

        DB::table('direction')->insert([
            [ "code" => "02", "libelle" => "DRAS" ],
            [ "code" => "03", "libelle" => "DRYOP" ],
            [ "code" => "04", "libelle" => "DRAN" ],
            [ "code" => "21", "libelle" => "DRE" ],
            [ "code" => "22", "libelle" => "DRABO" ],
            [ "code" => "23", "libelle" => "DRSO/DRSP" ],
            [ "code" => "24", "libelle" => "DRBC" ],
            [ "code" => "25", "libelle" => "DRSE" ],
            [ "code" => "41", "libelle" => "DRC" ],
            [ "code" => "42", "libelle" => "DRCO" ],
            [ "code" => "43", "libelle" => "DRN" ],
            [ "code" => "44", "libelle" => "DRO" ],
            [ "code" => "45", "libelle" => "DRCS" ],
        ]);

        DB::table('exploitation')->insert([
            [ "code" => "021", "libelle" => "KOUMASSI", "direction_id" => 1 ],
            [ "code" => "022", "libelle" => "MARCORY", "direction_id" => 1 ],
            [ "code" => "023", "libelle" => "PORT-BOUET", "direction_id" => 1 ],
            [ "code" => "024", "libelle" => "TREICHVILLE", "direction_id" => 1 ],
            [ "code" => "031", "libelle" => "LOCODJORO", "direction_id" => 2 ],
            [ "code" => "032", "libelle" => "NIANGON", "direction_id" => 2 ],
            [ "code" => "033", "libelle" => "ATTIE", "direction_id" => 2 ],
            [ "code" => "034", "libelle" => "LOKOA", "direction_id" => 2 ],
            [ "code" => "041", "libelle" => "ADJAME NORD", "direction_id" => 3 ],
            [ "code" => "042", "libelle" => "ADJAME SUD", "direction_id" => 3 ],
            [ "code" => "043", "libelle" => "COCODY", "direction_id" => 3 ],
            [ "code" => "044", "libelle" => "DEUX PLATEAUX", "direction_id" => 3 ],
            [ "code" => "045", "libelle" => "DIBY", "direction_id" => 3 ],
            [ "code" => "046", "libelle" => "BINGERVILLE", "direction_id" => 3 ],
            [ "code" => "211", "libelle" => "ABENGOUROU", "direction_id" => 4 ],
            [ "code" => "212", "libelle" => "AGNIBILEKRO", "direction_id" => 4 ],
            [ "code" => "213", "libelle" => "BONDOUKOU", "direction_id" => 4 ],
            [ "code" => "215", "libelle" => "BOUNA", "direction_id" => 4 ],
            [ "code" => "217", "libelle" => "TANDA", "direction_id" => 4 ],
            [ "code" => "218", "libelle" => "ADZOPE", "direction_id" => 4 ],
            [ "code" => "219", "libelle" => "AKOUPE", "direction_id" => 4 ],
            [ "code" => "221", "libelle" => "ABOBO CENTRE", "direction_id" => 5 ],
            [ "code" => "222", "libelle" => "ABOBO DOKUI", "direction_id" => 5 ],
            [ "code" => "223", "libelle" => "PLATEAU DOKUI", "direction_id" => 5 ],
            [ "code" => "224", "libelle" => "ANYAMA", "direction_id" => 5 ],
            [ "code" => "231", "libelle" => "GAGNOA", "direction_id" => 6 ],
            [ "code" => "232", "libelle" => "DIVO", "direction_id" => 6 ],
            [ "code" => "233", "libelle" => "OUME", "direction_id" => 6 ],
            [ "code" => "236", "libelle" => "SOUBRE", "direction_id" => 6 ],
            [ "code" => "238", "libelle" => "SINFRA", "direction_id" => 6 ],
            [ "code" => "239", "libelle" => "GUEYO", "direction_id" => 6 ],
            [ "code" => "23A", "libelle" => "GUIBEROUA", "direction_id" => 6 ],
            [ "code" => "23B", "libelle" => "HIRE", "direction_id" => 6 ],
            [ "code" => "23C", "libelle" => "LAKOTA", "direction_id" => 6 ],
            [ "code" => "23D", "libelle" => "OURAGAHIO", "direction_id" => 6 ],
            [ "code" => "23E", "libelle" => "BUYO", "direction_id" => 6 ],
            [ "code" => "234", "libelle" => "SAN-PEDRO", "direction_id" => 6 ],
            [ "code" => "235", "libelle" => "SASSANDRA", "direction_id" => 6 ],
            [ "code" => "237", "libelle" => "TABOU", "direction_id" => 6 ],
            //[ "code" => "2", "libelle" => "", "direction_id" =>  ],
        ]);

        DB::table('typedefaut')->insert([
            [ "code" => "H1", "nature" => "Toiture à la masse" ],
            [ "code" => "H2", "nature" => "Mur à la masse" ],
            [ "code" => "H3", "nature" => "Robinet à la masse" ],
            [ "code" => "H5", "nature" => "Potelet à la masse" ],
            [ "code" => "I2", "nature" => "Tableau P2, P4 en court-circuit" ],
            [ "code" => "I4", "nature" => "Tableau P2, P4 décroché" ],
            [ "code" => "I6", "nature" => "Fusible débroché au coffret à fusibles" ],
            [ "code" => "I9", "nature" => "Compteur classique en court-circuit" ],
            [ "code" => "I10", "nature" => "Compteur classique volé" ],
        ]);
    }
}
